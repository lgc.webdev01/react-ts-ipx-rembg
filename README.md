# react-ts-ipx-rembg

In the project root folder, run:

mkcert localhost

"start": "export HTTPS=true&&SSL_CRT_FILE=localhost.pem&&SSL_KEY_FILE=localhost-key.pem react-scripts start",



openssl req -x509 -nodes -new -sha256 -days 390 -newkey rsa:2048 -keyout "RootCA.key" -out "RootCA.pem" -subj "/C=de/ST=State/L=City/O=Organization/CN=localhost.local"
openssl x509 -outform pem -in "RootCA.pem" -out "RootCA.crt"

openssl req -new -nodes -newkey rsa:2048 -keyout localhost.key -out localhost.csr -subj "/C=de/ST=State/L=City/O=Organization/CN=localhost.local"
openssl x509 -req -sha256 -days 1024 -in localhost.csr -CA RootCA.pem -CAkey RootCA.key -CAcreateserial -extfile vhosts_domains.ext -out localhost.crt
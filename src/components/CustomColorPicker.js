import React, { FC } from 'react';
import PropTypes from 'prop-types'
import reactCSS from 'reactcss'
import merge from 'lodash/merge'
import { CustomPicker } from 'react-color'
import { Saturation, Alpha, Checkboard, EditableInput, Hue } from 'react-color/lib/components/common'

import UnfoldMoreHorizontalIcon from '@icons/material/UnfoldMoreHorizontalIcon'
import { ColorResult } from 'react-color';
 
type ColorPickerProps = {
  toggleViews: () => void;
  view: string;
  onChange: (color: ColorResult | any) => void;
  disableAlpha: boolean;
  rgb: {
    r: number;
    g: number;
    b: number;
    a?: number;
  };
  hsl: {
    h: number;
    s: number;
    l: number;
    a?: number;
  };
  hsv: {
    h: number;
    s: number;
    v: number;
    a?: number;
  };
  hex: string;
  renderers: any;
  styles?: any;
  className?: string;
  defaultView?: string;
};
export const ColorPicker: FC<ColorPickerProps> = ({
  toggleViews,
  view,
  onChange,
  disableAlpha,
  rgb,
  hsl,
  hsv,
  hex,
  renderers,
  styles: passedStyles = {},
  className = '',
  defaultView,
}) => {

  const styles = reactCSS(merge({
    'default': {
      container: {
        backgroundColor: '#fff',
        borderRadius: '8px',
        border: '1px solid #fff',
        padding: '10px',
        display: 'flex',
        flexDirection: 'column',

      },
      circle:{
        circleSpacing:{

        }
      },

      svg:{
        color: '#999',
        position: 'absolute',
        right: '10px',

      },
      field:{
        float:'left',
      },
      fields:{
        display:'flex',
      },
      input: {
        border: '0px',
        borderBottom: '1px solid',
        width: '30%',
        alignItems: 'right',fontSize: '11px',
      },
      inputhex: {
        border: '0px',
        borderBottom: '1px solid',
        width: '50%',
        alignItems: 'right',fontSize: '11px',
      },
      label: {
        fontSize: '11px',
        color: '#999', float:'left',
      },
      saturation: {
        width: '100%',
        paddingBottom: '35%',
        position: 'relative',
        borderRadius: '2px 2px 0 0',
        overflow: 'hidden',
      },
      slider: {
        width: '15px',
        height: '15px',
      },
      
      body: {
        padding: '16px 16px 12px',
      },
      controls: {
        display: 'flex',
      },
      color: {
        width: '32px',
      },
      swatch: {
        marginTop: '6px',
        width: '16px',
        height: '16px',
        borderRadius: '8px',
        position: 'relative',
        overflow: 'hidden',
      },
      active: {
        absolute: '0px 0px 0px 0px',
        borderRadius: '8px',
        boxShadow: 'inset 0 0 0 1px rgba(0,0,0,.1)',
        background: `rgba(${ rgb.r }, ${ rgb.g }, ${ rgb.b }, ${ rgb.a })`,
        zIndex: '2',
      },
      toggles: {
        flex: '1',
      },
      hue: {
        height: '25px',
        position: 'relative',
        marginBottom: '8px',
   
      },
      Hue: {
        radius: '4px',
        
      },
      alpha: {
        height: '10px',
        position: 'relative',
      },
      Alpha: {
        radius: '2px',
      },
    },
  }, passedStyles))

   
  let fields
  if (view === 'hex') {
    fields = (<div style={ styles.fields } className="flexbox-fix">
      <div style={ styles.field }>
        <EditableInput
          style={{ input: styles.inputhex, label: styles.label }}
          label="hex" value={ hex }
          onChange={ onChange }
        />
      </div>
              </div>)
  } else if (view=== 'rgb') {
    fields = (<div style={ styles.fields } className="flexbox-fix">
      <div style={ styles.field }>
        <EditableInput
          style={{ input: styles.input, label: styles.label }}
          label="r"
          value={ rgb.r }
          onChange={ onChange }
        />
      </div>
      <div style={ styles.field }>
        <EditableInput
          style={{ input: styles.input, label: styles.label }}
          label="g"
          value={ rgb.g }
          onChange={ onChange }
        />
      </div>
      <div style={ styles.field }>
        <EditableInput
          style={{ input: styles.input, label: styles.label }}
          label="b"
          value={ rgb.b }
          onChange={ onChange }
        />
      </div>
              </div>)
  }
  const inlineStyles = {
    container: {
      boxShadow: 'rgba(0, 0, 0, 0.2) 0px 3px 1px -2px, rgba(0, 0, 0, 0.14) 0px 2px 2px 0px, rgba(0, 0, 0, 0.12) 0px 1px 5px 0px',
      display: 'flex',
      flexDirection: 'column',
      height: 282,
      width: 200,
    },
    pointer: {
      width: '20px',
      height: '20px',
      borderRadius: '50%',
      transform: 'translate(-9px, -1px)',
      backgroundColor: 'rgb(248, 248, 248)',
      boxShadow: '0 1px 4px 0 rgba(0, 0, 0, 0.37)',
    },
    slider: {
      marginTop: '1px',
      width: '4px',
      borderRadius: '1px',
      height: '8px',
      boxShadow: '0 0 2px rgba(0, 0, 0, .6)',
      background: '#fff',
      transform: 'translateX(-2px)'
    },
    saturation: {
      width: '100%',
      paddingBottom: '75%',
      position: 'relative',
      overflow: 'hidden',
    },
    swatchSquare: {
      minWidth: 20,
      minHeight: 20,
      margin: '1px 2px',
      cursor: 'pointer',
      boxShadow: '0 0 2px rgba(0, 0, 0, .6)',
    }
  }
  const showHighlight = (e) => {
    e.currentTarget.style.background = 'black'
  }
  const CustomPointer = () => {
    return (
      <div style={ inlineStyles.pointer } />
    )
  }
  const hideHighlight = (e) => {
    e.currentTarget.style.background = 'grey'
  }

  return (
    <div style={ styles.container }>
      <div style={ styles.circle }>
        <Circle
         circleSize={20}
         colors={[ '#F44E3B', '#FE9200', '#FCDC00', '#DBDF00' ]}
          pointer={ CustomPointer }
          onChange={ onChange }
        />
      </div>
      <div style={ styles.saturation }>
        <Saturation
         
          hsl={ hsl }
          hsv={ hsv }
          pointer={ CustomPointer }
          onChange={ onChange }
        />
      </div>
      <div style={ styles.hue }>
        <Hue hsl={ hsl } onChange={ onChange } />
      </div>

      <div style={{ display: 'flex', textAlign: 'right', 'padding': '0px' }}>
        <div style={{ display: 'flex', textAlign: 'left', 'padding': '0px' }}>{fields}</div>
        <div style={ styles.toggle }>
          <div style={ styles.icon } onClick={ toggleViews } ref={ (icon) => icon = icon }>
            <UnfoldMoreHorizontalIcon
              style={ styles.svg }
              onMouseOver={ showHighlight }
              onMouseEnter={ showHighlight }
              onMouseOut={ hideHighlight }
            />
          </div>
        </div>
      </div>
    </div>
  )
}

export default CustomPicker(ColorPicker)

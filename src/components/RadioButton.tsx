import React from "react";
 

import { InputElementProps } from "../utils/InputInterface";

 

const RadioButton = ({
   id, 
   disabled = false, 
   key,
   value,
   defaultChecked
}: InputElementProps) => {
   return (
       
          <input 
             type="radio" 
             id={id} 
             disabled={disabled} 
             name={key}
             value={value}
             defaultChecked={defaultChecked}
          />
     
   );
};
 export default RadioButton;
import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate as useHistory } from 'react-router-dom';
import './Cart.css'
import content from './translate';
import Header from './Header';
const Reset_body = () => {
  document.body.classList.add('reset_body_cart');
  document.body.classList.remove('reset_body');
  return <div>{''}</div>;
};

function Cart(props: { language: string; }) {


  const currentlanguage = content[props.language];
  const [previewImage, setPreviewImage] = useState<string | undefined>(
    undefined
  );
  const history = useHistory();
  const location = useLocation();
  const [cartoption, setCartOption] = React.useState('pro');
  const [showDownloadButton, setShowDownloadButton] = React.useState(false);

  const handleChange = (event: { target: { value: any; }; }) => {
    setCartOption(event.target.value)
  }
  const handleSelect = (selection: string) => {
    setCartOption(selection);
  }
  const handleDownload = (event: { preventDefault: () => void }) => {
    // Download the received image
    event.preventDefault();
    const link = document.createElement('a');
    link.href = previewImage ? previewImage : '';
    link.download = 'image.png';
    link.click();
  };
  const handleGoToCart = (event: { preventDefault: () => void }) => {
    // Go to the cart the received image
    event.preventDefault();
    const link = document.createElement('a');
    if(cartoption === 'unlock'){
        link.href = 'https://store.inpixio.com/clickgate/join.aspx?ref=inpixio.upclick.com%2F1&ujid=9xcFynmFiy0%3D&culture=en';
     } else {
        link.href = 'https://store.inpixio.com/clickgate/join.aspx?ref=inpixio.upclick.com%2F1&ujid=uuM6zngZ%2Fdo%3D&culture=en';
      }
    link.click();
  };


  useEffect(() => {
    if (location.state?.type && location.state.type === 'download-sd') {
      setShowDownloadButton(true);
    } else {
      setShowDownloadButton(false);
    }
    if (!location.state?.image) {
      history('/', { state: { message: 'please chose a file' } });
      // console.log(location)
    } else {
      setPreviewImage(location.state.image);
    }
  }, [currentlanguage,location]);
  return (<>
    <Header />
    <main id="page">
      <div className="container v2 ">
        <div className="cart-steps">
          <div className="cart-steps_step">
            <img src="https://www.inpixio.com/rb-tool/assets/img/icon-check-active.svg" width="14" height="14" alt="Check Icon" />
            <span>Image Completed</span>
          </div>
          <div className="cart-steps_step">
            <img src="https://www.inpixio.com/rb-tool/assets/img/icon-check-active.svg" width="14" height="14" alt="Check Icon" />
            <span>Plan</span>
          </div>
          <div className="cart-steps_step">
            <img src="https://www.inpixio.com/rb-tool/assets/img/icon-check-inactive.svg" width="14" height="14" alt="Check Icon" />
            <span>Payment Details</span>
          </div>
          <div className="cart-steps_step">
            <img src="https://www.inpixio.com/rb-tool/assets/img/icon-check-inactive.svg" width="14" height="14" alt="Check Icon" />
            <span>Completed</span>
          </div>
        </div>
        <div className="cart-body">
          <h1>{currentlanguage?.selectaplan}</h1>
          <div className="cart-body_cols">
            <div className="cart-body_col-left">
              <div className="result-image">

                {previewImage && (
                  <img
                    src={previewImage}
                    alt=""
                    id="my-preview-without-background" />
                )}
              </div>
              <div className="trustpilot">
                <img src="https://www.inpixio.com/rb-tool/assets/img/trustpilot.svg" width="84" height="39" alt="Trustpilot" />
                <span>{currentlanguage?.OurCustomersays} <strong>{currentlanguage?.Excellent} 4.3 </strong>{currentlanguage?.outof} <strong>7,290 {currentlanguage?.review}</strong></span>
              </div>
            </div>
            <div className="cart-body_col-right">
              <form action="" className="rb-tool-checkout">
                <div className="cart-option selected">
                  <div className="discount-tag">
                    <span>50%</span>
                    <span>OFF</span>
                  </div>
                  <div className="cart-option_head" onClick={() => handleSelect('pro')}>
                    <label className="cart-option_select">
                      <input
                        type="radio"
                        value="pro"
                        checked={cartoption === 'pro'}
                        onChange={handleChange}
                      />
                      <h2>Pro Access</h2>
                    </label>
                    <div className="cart-option_price">
                      <strong>{currentlanguage?.discountPricePro}</strong>
                      <s>{currentlanguage?.fullPricePro}</s>
                    </div>
                  </div>
                  <div className="cart-option_body" onClick={() => handleSelect('pro')}>
                    <h3>{currentlanguage?.included}</h3>
                    <ul className="checks-ul">
                      <li>{currentlanguage?.included1}</li>
                      <li>{currentlanguage?.included2}</li>
                      <li>{currentlanguage?.included3}</li>
                      <li>{currentlanguage?.included4}</li>
                      <li>{currentlanguage?.included5}</li>
                      <li>{currentlanguage?.included6}</li>
                    </ul>
                  </div>
                </div>
                <div className="cart-option" onClick={() => handleSelect('unlock')}>
                  <div className="cart-option_head">
                    <label className="cart-option_select">
                      <input
                        type="radio"
                        value="unlock"
                        checked={cartoption === 'unlock'}
                        onChange={handleChange}
                      />
                      <h2>{currentlanguage?.unlockOne}</h2>
                    </label>
                    <div className="cart-option_price">
                      <strong>{currentlanguage?.priceUnlockOne}</strong>
                    </div>
                  </div>
                  <div className="cart-option_body">
                    <ul className="checks-ul">
                      <li>{currentlanguage?.unlockOneincluded}</li>
                    </ul>
                  </div>
                </div>
                <div className="rb-tool-checkout_btns">
                  {showDownloadButton === true &&
                    <a href="#" className="btn-cart download" onClick={handleDownload}>Download</a>}

                  <button className="btn-cart" onClick={handleGoToCart}>Continue</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <Reset_body />
    </main><footer className='footer-cart'>
      <div className="menu">
        <ul className="links">
          <li className="first"><a target="_blank" href="/legal-notice/">Legal Notice</a></li>
          <li><a target="_blank" href="/about-inpixio/">About Us</a></li>
          <li><a target="_blank" href="/privacy-policy/">Privacy Policy</a></li>
          <li><a target="_blank" href="/cookie-policy/">Cookie Policy</a></li>
          <li><a target="_blank" href="/ccpa/">CCPA</a></li>
          <li><a target="_blank" href="/support/">Support</a></li>
          <li className="last"><a target="_blank" href="/eula/">EULA</a></li>
        </ul> </div>
      <div className="copyright">Transaction processed by Upclick Inc. ( 7075 Place Robert- Joncas Suite 142 , Montreal, Quebec, Canada, H4M-2Z2 ) E-commerce services provided by Upclick, an official LULU Software (7270356 Canada inc) reseller.</div>
    </footer></>
  );
}
export default Cart;

import React from "react";
type TabContentProps = {
    activeTab: number;
     
    id: number;
    children: React.ReactNode;
  }; 
const TabContent:React.FunctionComponent <TabContentProps> = (props)=>{
    const { id, activeTab, children } = props;
    const isActive = activeTab === id;
 return (
   activeTab === id ? <div className="TabContent {activeTab === 1 ? 'active' : ''}" >
     { children }
   </div>
   : null
 );
};
 
export default TabContent;

 
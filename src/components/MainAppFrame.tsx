import React, { useState, useEffect, useRef, SetStateAction, RefObject } from 'react';
import { useLocation, useNavigate as useHistory } from 'react-router-dom';
import Cookies from "js-cookie";
import './MainAppFrame.css';
import selector from '../assets/Color-selector.png';
import { createPost } from '../utils';
import TabNavItem from './TabNavItem';
import TabContent from './TabContent';
import Canvas from './Canvas';
import ColorPicker from './ColorPicker';
import Footer from './Footer';
import axios from 'axios';

const Spinner = () => {
  // document.body.classList.add('upload-loading');
  return <div className="modal-overlay" ></div>;
};
const MainAppFrame = () => {
  const [imageSrc, setImageSrc] = useState<string>("");
  const [color, setColor] = useState('null');
  const location = useLocation();
  const history = useHistory();


  const [loading, setLoading] = useState<boolean>(true); // new state variable
  const [error, setError] = useState<string | null>(null);
  const [activeTab, setActiveTab] = useState(1);
  const [uploadedImage, setUploadedImage] = useState<string | undefined>(undefined);
  const [sourceDimension, setSourceDimension] = useState({ width: 0, height: 0 });
  const [showColorPicker, setShowColorPicker] = useState(false);
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [isProductActived, setIsProductActived] = useState(false);
  const [image, setImage] = useState<File | null>();
  const inputRef: RefObject<HTMLInputElement> = useRef(null);
  const canvasRef = useRef<HTMLCanvasElement | null>(null);
  const [sendImage, setSendImage] = useState(false);


  useEffect(() => {
    const getProductsByToken = async () => {
      try {
        const url = "https://myaccount.inpixio.com/api/account/owner-products";
        const response = await fetch(url, {
          method: "GET",
          mode: 'cors',
          credentials: 'include',
          redirect: "follow"
        });

        if (response.status === 400) {
          throw new Error("Status code error: " + response.status);
        }

        const data = await response.json();

        const filteredProducts = data.filter((product: any) =>
          ['P031528', 'P030791'].includes(product.uid)
        ).filter((product: any) => new Date(product.endDate) > new Date());

        if (filteredProducts.length > 0) {
          setIsProductActived(true);
        }

        setIsAuthenticated(true);

        return filteredProducts;
      } catch (error:any) {
        setError(error);
      }
    };

    const getToken = async () => {
      const result = await getProductsByToken();
      return result;
    };

    const cleanup = () => {
      if (uploadedImage) {
        setImageSrc(uploadedImage);
      }
    };

    getToken();

    return cleanup;
  }, [uploadedImage]);

  const toggleColorPicker = () => {
    setShowColorPicker(!showColorPicker);
  };
 
  useEffect(() => {
    if (!location.state?.image && !location.state?.id) {
      history('/', { state: { message: 'please chose a file' } });
    } else if (location.state?.id) {
      setLoading(true);
      setImageSrc('https://inpixio-removebg-app.s3.amazonaws.com/remove-bg/static/images/' + location.state.id + '_after.png');

      setUploadedImage('https://statics.inpixio.com/remove-bg/static/images/' + location.state.id + '.jpg');;

      setLoading(false);
      setActiveTab(2);
      handleColorChange('transparent')
      /*  setImageSrc('https://inpixio-removebg-app.s3.amazonaws.com/remove-bg/static/images/' + location.state.id + '_after.png');
       setUploadedImage('https://statics.inpixio.com/remove-bg/static/images/' + location.state.id + '.jpg');;
    */
    } else if (location.state?.image) {

      setLoading(true); // set loading state to true

      setUploadedImage(URL.createObjectURL(location.state.image));
      // resizeImage(URL.createObjectURL(location.state.image), 600, 400);
      setSourceDimension(location.state.dimension);

      const postToProxy = async () => {
        try {
          const response = await createPost(location.state.image);
          setImageSrc('data:image/png;base64,' + response.data.image);
        } catch (err: any) {
          setError(err.message);
        } finally {
          handleColorChange('transparent');
          window['dataLayer'].push({ 'event': 'image_processed' });
          setActiveTab(2);
          setLoading(false);
        }
      };
      // Call function immediately

      postToProxy();


    }
  }, [location]);

  const handleFileUpload = (
    files: FileList | null,
    setError: (err: string) => void,
    setImage: (img: File) => void,
    history: any
  ) => {

    if (files && files.length > 0) {
      const file = files[0];
      let img = new Image()
      let img_size = { width: 0, height: 0 }
      img.src = window.URL.createObjectURL(file)
      img.onload = () => {
        img_size.width = img.width;
        img_size.height = img.height;
        try {
          if (!file) {
            setError('Please select an image');
          } else if (!file.type.startsWith('image')) {
            setError('Invalid file type. Select an image file');
          } else if (file.size > 125000000) {
            setError(
              'File is too large. Select an image below 5MB'
            );
          } else {
            setLoading(true);
            setImage(file); // Navigate to the processing route
            setUploadedImage(URL.createObjectURL(file));
            setImageSrc(URL.createObjectURL(file));
            // resizeImage(URL.createObjectURL(location.state.image), 600, 400);
            setSourceDimension(img_size);

            history('/remove-background', { state: { image: file, dimension: img_size } });

          }
        } catch (error) {
          console.log(error);
          setError('An error occurred while uploading the image');
        } finally {
          setLoading(false); // Hide the loading spinner
        }
      }

    }
  };
  const handleChartDownload = (canvasRef: string) => {
    const chartCanvas = document.getElementById(canvasRef) as HTMLCanvasElement;
    if (chartCanvas) {
       
            const url = chartCanvas.toDataURL("image/png");
            // Create a download link
            const link = document.createElement("a");
            link.download = "filename.png";
            link.href = url;
            link.click();
         
    } else {
        console.error(`Could not find canvas with id: ${canvasRef}`);
    }
};
  const handleChartDownloadold = (canvasRef: string) => {
    const chartCanvas = document.getElementById(canvasRef) as HTMLCanvasElement;
    if (chartCanvas) {
      const ctx = chartCanvas.getContext("2d");
      if (ctx) {
        const chartRef = ctx.canvas;
        const url = chartRef.toDataURL("image/png");
        const link = document.createElement("a");
        link.download = "filename.png";
        link.href = url;
        link.click();
      } else {
        console.error("Could not get context from canvas");
      }
    } else {
      console.error(`Could not find canvas with id: ${canvasRef}`);
    }
  };

  const handleUpload = async (e: { target: { files: FileList | null } }) => {
    setError(null); // Reset any previous errors
    setLoading(true); // Show the loading spinner
    // const file = e.target?.files[0];
    const { files } = e.target;
    handleFileUpload(files, setError, setImage, history);
  };

  const styles = {
    popup: {
      display: loading ? 'inline-block' : 'none',
      opacity: loading ? '1' : '0',
    },
    image: {
      maxWidth: '100%',
      width: '100%',
      opacity: loading ? '1' : '1',
    },
    prothumb: {
      maxWidth: '80px !important',
      maxHeight: '80px !important',
      opacity: loading ? '1' : '1'
    },
    upload__choose: {
      height: '30px'
    },

    prothumb_img: {


    },
    download: {
      opacity: loading ? '1' : '1',
      alignItems: 'left',

    }
  };

  const handleGoToCart = (event: { preventDefault: () => void }) => {
    let track_vars = Cookies.get("track_vars");
    if (!track_vars) track_vars = "mkey1=Direct";
    event.preventDefault();
    const link = document.createElement('a');
    link.href = 'https://store.inpixio.com/clickgate/join.aspx?ref=inpixio.upclick.com%2F1&ujid=8olIWm%2Fhzpw%3D&culture=en&' + track_vars;
    link.click();
  };

  const handleColorChange = (newColor: any) => {
    console.log(newColor);
    if (activeTab == 1) {
      setActiveTab(2);
    }
    setColor(newColor);

  };

  const sendIssueOpen = (e: { preventDefault: () => void; }) => {
    // setIsOverlayVisible(false);
    const reportIssueOverlay = document.getElementById('report-issue-overlay')
    reportIssueOverlay?.classList.add('active')
    e.preventDefault()
  };
  const sendIssueClose = (e: { preventDefault: () => void; }) => {
    const reportIssueOverlay = document.getElementById('report-issue-overlay')
    reportIssueOverlay?.classList.remove('active')


    e.preventDefault()

    // setIsOverlayVisible(false);
  };

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    // Send the form data to the server
    const formData = new FormData(event.currentTarget);

    if (sendImage) {
      // formData.append("image", imageSrc);
    }
    fetch('https://services.avanquest.com/inpixio/remove-bg-support.cfm', {
      method: 'POST',
      body: formData
    })
      .then((response) => {
        console.log('Form submitted successfully:', response);
        sendIssueClose(event);
      })
      .catch((error) => {
        console.error('Error submitting form:', error);
        sendIssueClose(event);
      });

  };

  const handleCheckboxChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      // Set the pre-saved image data
      setSendImage(true);
    } else {
      setSendImage(false);
    }
  };
  return (
    <>
      {loading === true && (

        <Spinner />

      )}
      <header id="header">
        <div className="container v2 flex">
          <a href="/remove-bg"><img
            src="https://www.inpixio.com/remove-background/images/new/logo-inpixio.svg"
            width="168"
            height="52"
            alt="inPixio"
            className="logo" />
          </a>
          <div className="header-menu">
            <div className="header-menu_pro">
              <a href="#" className="btn outline pro-upg" onClick={handleGoToCart}>
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" className="icon inline">
                  <g>
                    <rect width="24" height="24" fill="none" />
                  </g>
                  <g transform="translate(1.592 5)">
                    <path d="M19,3H5L2,9,12,21,22,9ZM9.62,8l1.5-3h1.76l1.5,3ZM11,10v6.68L5.44,10Zm2,0h5.56L13,16.68Zm6.26-2H16.61l-1.5-3h2.65ZM6.24,5H8.89L7.39,8H4.74Z" transform="translate(-2 -3)" fill="#51c959" className="fill" />
                  </g>
                </svg>
                <span className="inline">UPGRADE TO PRO</span>
              </a>
            </div>
            <a href="https://myaccount.inpixio.com/" className="account-btn" target="_self">
              <img src="https://www.inpixio.com/rb-tool/assets/img/icon-account.svg" width="24" height="24" alt="Account Icon" />
              {isAuthenticated === false && <span className="account-btn_status"></span>}
            </a>
          </div>
        </div>
      </header>
      <main id="page">
        <div className="container v2 container-main uploaded-container">
          <div className="c-upload__choose" style={styles.upload__choose}>
            <p>{''}</p>
          </div>
          <div className="uploaded-result_wrap">
            <div className="uploaded-result_image-wrap" id="preview_handler">
              <div className="uploaded-result_navigation">
                <ul className="nav">
                  <TabNavItem title="Original" id={1} activeTab={activeTab} setActiveTab={setActiveTab} />
                  <TabNavItem title="Remove Background" id={2} activeTab={activeTab} setActiveTab={setActiveTab} />
                </ul>
              </div>
              <TabContent id={1} activeTab={activeTab} >
                <div className="uploaded-result_image-wrap" id="source_handler" >
                  <div className="uploaded-result_image">
                    {uploadedImage && (
                      <img
                        src={uploadedImage}
                        alt=""
                        id="image-with-background"
                        style={styles.image} />
                    )}
                    <div className="loading-spinner" style={styles.popup}>
                      <div></div>
                      <div></div>
                      <div></div>
                      <div></div>
                    </div>
                  </div>
                </div>
              </TabContent>
              <TabContent id={2} activeTab={activeTab}>
                <div className="uploaded-result_image">
                  <Canvas
                    sourceDimension={sourceDimension} src={imageSrc} width={720} height={480} style={styles.image} backgroundColor={color} />
                </div>
              </TabContent>
              <button id="report-issue-btn" onClick={sendIssueOpen}>
                <svg className="inline icon" id="improvement_icon_mobile" data-name="improvement icon mobile" xmlns="http://www.w3.org/2000/svg" width="19.994" height="19.994" viewBox="0 0 19.994 19.994">
                  <path id="Tracé_11501" data-name="Tracé 11501" d="M10,0a10,10,0,1,0,10,10A10.008,10.008,0,0,0,10,0Zm0,18.913A8.915,8.915,0,1,1,18.913,10,8.888,8.888,0,0,1,10,18.913Z" fill="#fff" />
                  <ellipse id="Ellipse_1072" data-name="Ellipse 1072" cx="1.062" cy="1.062" rx="1.062" ry="1.062" transform="translate(5.663 6.701)" fill="#fff" />
                  <ellipse id="Ellipse_1073" data-name="Ellipse 1073" cx="1.062" cy="1.062" rx="1.062" ry="1.062" transform="translate(12.364 6.701)" fill="#fff" />
                  <path id="Tracé_11502" data-name="Tracé 11502" d="M5.285,3.373a6.428,6.428,0,0,0,5.324-2.8L9.686,0A5.324,5.324,0,0,1,4.7,2.278,5.289,5.289,0,0,1,.922,0L0,.577A6.4,6.4,0,0,0,4.574,3.333,6.405,6.405,0,0,0,5.285,3.373Z" transform="translate(15.301 15.52) rotate(180)" fill="#fff" />
                </svg>

                <span className="inline"> AI did not work?</span></button>
            </div>
            <div className="uploaded-result_actions">

              <small>Full resolution image</small>
              <div className="uploaded-result_actions-block">
                <a href="#" className="btn med" onClick={handleGoToCart}>
                  <svg
                    className="inline icon"
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                  >
                    <g>
                      <rect width="24" height="24" fill="none" />
                    </g>
                    <g transform="translate(1.592 5)">
                      <path
                        d="M19,3H5L2,9,12,21,22,9ZM9.62,8l1.5-3h1.76l1.5,3ZM11,10v6.68L5.44,10Zm2,0h5.56L13,16.68Zm6.26-2H16.61l-1.5-3h2.65ZM6.24,5H8.89L7.39,8H4.74Z"
                        transform="translate(-2 -3)"
                        fill="#ffffff" />
                    </g>
                  </svg>
                  <span className="inline">Download HD</span>
                </a>
                <small className='download-icon' onClick={() => handleChartDownload('user-canvas-image-without-background')}>Download low resolution</small>
              </div>
              <div className="uploaded-result_desktop-block">
                <div className="uploaded-result_desktop-block_inner">
                  <div className="pro-thumb pro-thumb-1"

                    onClick={() => handleColorChange('transparent')}>
                    {imageSrc && loading===false &&  (
                      <img
                        src={imageSrc}
                        alt=""
                        id="my-image-without-background-pro-thumb-2"
                        style={styles.prothumb} />
                    )}
                  </div>
                  <div className="pro-thumb pro-thumb-2"
                    onClick={() => handleColorChange('#FFFFFF')}>
                    {imageSrc && loading===false && (
                      <img
                        src={imageSrc}
                        alt=""
                        id="my-image-without-background-pro-thumb-2"
                        style={styles.prothumb} />
                    )}
                  </div>
                  <div className="pro-thumb pro-thumb-3"
                    onClick={() => handleColorChange('#000000')} >
                    {imageSrc && loading===false && (
                      <img
                        src={imageSrc}
                        alt=""
                        id="my-image-without-background-pro-thumb-3"
                        style={styles.prothumb} />
                    )}
                  </div>
                </div>

                <small style={styles.download} className="small-left">
                  {!showColorPicker && (
                    <><small onClick={toggleColorPicker} className='select-text'>Select Color</small>
                      <img src={selector} alt="" className="download-icon"
                        onClick={toggleColorPicker} />
                    </>
                  )}
                </small>
                {showColorPicker && (
                  <ColorPicker onChange={handleColorChange} value={''} />

                )}
              </div>
            </div>
          </div>

        </div>
      </main>
      <div id="report-issue-overlay" className="pop-overlay">
        <div className="pop-overlay_inner">
          <div className="report-issue-form">
            <div className="report-issue-form_top">
              <button id="report-issue-close" onClick={sendIssueClose}>
                <img src="https://www.inpixio.com/rb-tool/assets/img/icon-close.svg" width="24" height="24" alt="Close Popup" /></button>
              <div className="title">We’re working on improvements!</div>
              <p>Let us use your image to improve the output <br />quality of our technology</p>
            </div>
            <form onSubmit={handleSubmit} id="report-issue">
              <div className="faces">
                <label id="face-sad" className="feedback-quality-face">
                  <input type="radio" name="feedback_face" value="sad" />
                  <span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48">
                      <g id="Smiley_not_Happy" data-name="Smiley not Happy" transform="translate(-20732 773)">
                        <g id="Groupe_33692" data-name="Groupe 33692" transform="translate(20732 -773)">
                          <path id="Tracé_11501" data-name="Tracé 11501" d="M24,0A24,24,0,1,0,48,24,24.027,24.027,0,0,0,24,0Zm0,45.405A21.4,21.4,0,1,1,45.405,24,21.337,21.337,0,0,1,24,45.405Z" fill="rgba(0,0,0,0.6)" />
                          <ellipse id="Ellipse_1072" data-name="Ellipse 1072" cx="2.55" cy="2.55" rx="2.55" ry="2.55" transform="translate(13.595 16.087)" fill="rgba(0,0,0,0.6)" />
                          <ellipse id="Ellipse_1073" data-name="Ellipse 1073" cx="2.55" cy="2.55" rx="2.55" ry="2.55" transform="translate(29.683 16.087)" fill="rgba(0,0,0,0.6)" />
                          <path id="Tracé_11502" data-name="Tracé 11502" d="M12.687,8.1a15.432,15.432,0,0,0,12.78-6.713L23.255,0A12.781,12.781,0,0,1,11.283,5.469,12.7,12.7,0,0,1,2.213,0L0,1.384A15.37,15.37,0,0,0,10.981,8,15.377,15.377,0,0,0,12.687,8.1Z" transform="translate(36.733 37.259) rotate(180)" fill="rgba(0,0,0,0.6)" />
                        </g>
                      </g>
                    </svg>
                  </span>
                </label>
                <label id="face-meh" className="feedback-quality-face active">
                  <input type="radio" name="feedback_face" value="meh" onChange={(e) => { e.target.value; }} defaultChecked/>
                  <span>
                    <svg id="Group_33693" data-name="Group 33693" xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48">
                      <g id="Smiley_medium-2" data-name="Smiley medium">
                        <g id="Group_33664-2" data-name="Group 33664" transform="translate(13.76 16.223)">
                          <ellipse id="Ellipse_1072-2" data-name="Ellipse 1072" cx="2.55" cy="2.55" rx="2.55" ry="2.55" fill="rgba(0,0,0,0.6)" />
                          <ellipse id="Ellipse_1073-2" data-name="Ellipse 1073" cx="2.55" cy="2.55" rx="2.55" ry="2.55" transform="translate(15.781)" fill="rgba(0,0,0,0.6)" />
                          <rect id="Rectangle_14435-2" data-name="Rectangle 14435" width="21.202" height="3.029" transform="translate(0.272 14.973)" fill="rgba(0,0,0,0.6)" />
                        </g>
                        <g id="cicle-2" data-name="cicle">
                          <path id="Path_11501-2" data-name="Path 11501" d="M24,0A24,24,0,1,0,48,24,24.027,24.027,0,0,0,24,0Zm0,45.405A21.4,21.4,0,1,1,45.405,24,21.337,21.337,0,0,1,24,45.405Z" fill="rgba(0,0,0,0.6)" />
                        </g>
                      </g>
                    </svg>
                  </span>
                </label>
                <label id="face-happy" className="feedback-quality-face">
                  <input type="radio" name="feedback_face" value="happy" />
                  <span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48">
                      <g id="Smilley_Happy" data-name="Smilley Happy" transform="translate(-20876 773)">
                        <path id="Union_69" data-name="Union 69" d="M0,24A24,24,0,1,1,24,48,24.027,24.027,0,0,1,0,24Zm2.594,0a21.269,21.269,0,0,0,2.439,9.914A21.4,21.4,0,0,0,45.406,24a21.285,21.285,0,0,0-2.094-9.227A21.4,21.4,0,0,0,2.594,24Zm19.68,13.2a15.062,15.062,0,0,1-10.812-6.734l2.179-1.407a12.328,12.328,0,0,0,20.719,0l2.179,1.407A15.066,15.066,0,0,1,23.955,37.3,14.632,14.632,0,0,1,22.274,37.2Zm7.348-18.553a2.595,2.595,0,1,1,2.594,2.6A2.594,2.594,0,0,1,29.622,18.648Zm-16.054,0a2.594,2.594,0,1,1,2.594,2.6A2.594,2.594,0,0,1,13.568,18.648Z" transform="translate(20876 -773)" fill="rgba(0,0,0,0.6)" />
                      </g>
                    </svg>
                  </span>
                </label>
              </div>
              <div className="report-issue-form_body">
                <input type="email" name="issue_email" id="issue-email" placeholder="Email Address" className="input" />
                <div className="textarea-wrap">
                  <textarea name="issue_thoughts" id="issue-thoughts" placeholder="Share you thought here" maxLength={2048} className="input"></textarea>
                  <small><span id="issue-char-count">0</span> / 2048</small>
                </div>
                <label>
                  <input
                    type="checkbox"
                    name="Send-my-image"
                    id="Send-my-image"
                    onChange={handleCheckboxChange}
                  />                  <strong>Send my image to improve the output quality</strong>
                </label>
                <div className="report-issue-form_btn-wrap">
                  <input type="submit" name="submit" value="SEND" className="btn blue" />
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div className="c-upload__choose" style={styles.upload__choose}>
        <p>{''}</p>
      </div>
      <Footer />
    </>
  );

};
export default MainAppFrame;




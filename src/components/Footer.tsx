import React from 'react';
 
 


const Footer = () => {

     

    return (
        <footer className='mainFrame'>
        <div className="logo">
          <img
            width="195"
            height="60"
            src="https://www.inpixio.com/static/lp/photostudio/EN/mb/image/logo.svg"
            alt="logo" />
        </div>
        <div className="menu">
          <ul className="links">
            <li className="first">
              <a target="_blank" href="https://www.inpixio.com/legal-notice/"
              >Legal Notice</a>
            </li>
            <li>
              <a target="_blank" href="https://www.inpixio.com/about-inpixio/"
              >About Us</a>
            </li>
            <li>
              <a target="_blank" href="https://www.inpixio.com/privacy-policy/"
              >Privacy Policy</a>
            </li>
            <li>
              <a target="_blank" href="https://www.inpixio.com/cookie-policy/"
              >Cookie Policy</a>
            </li>
            <li>
              <a target="_blank" href="https://www.inpixio.com/ccpa/">CCPA</a>
            </li>
            <li>
              <a target="_blank" href="https://www.inpixio.com/support/"
              >Support</a>
            </li>
            <li className="last">
              <a target="_blank" href="https://www.inpixio.com/eula/">EULA</a>
            </li>
          </ul>
        </div>
        <div className="copyright">
          © Copyright 2023 InPixio by Avanquest. All Rights Reserved.
        </div>
      </footer>
    );
};

export default Footer;
/*

const Header = () => {
  return (<header id="header">
      <div className="container v2 flex">
          <a href="/"><img
              src="https://www.inpixio.com/remove-background/images/new/logo-inpixio.svg"
              width="168"
              height="52"
              alt="inPixio" />
          </a>
      </div>
  </header>);
};
export default Header;
*/



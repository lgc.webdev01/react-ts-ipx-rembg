import React from 'react';
import PropTypes from 'prop-types'
import reactCSS from 'reactcss'
import merge from 'lodash/merge'
import { CustomPicker } from 'react-color'
import { ColorWrap, Saturation, Alpha, Checkboard, EditableInput, Hue } from 'react-color/lib/components/common'
import ChromeFields from 'react-color/lib/components/chrome/ChromeFields'
import ChromePointer from 'react-color/lib/components/chrome/ChromePointer'
import ChromePointerCircle from 'react-color/lib/components/chrome/ChromePointerCircle'
import UnfoldMoreHorizontalIcon from '@icons/material/UnfoldMoreHorizontalIcon'
import Circle from 'react-color/lib/components/circle/Circle'


export const MyPicker = ({ toggleViews, view = 'rgb', onChange, disableAlpha, rgb, hsl, hsv, hex, renderers,
  styles: passedStyles = {}, className = '', defaultView }) => {

    let handleChangeRGB = (data) => {
      console.log(data)
      if(data.r){
        rgb.r = data.r
      }
      if(data.g){
        rgb.g = data.g
      }
      if(data.b){
        rgb.b = data.b
      }
  
      onChange(rgb)
       
    };
  const styles = reactCSS(merge({
    'default': {
      container: {
        backgroundColor: '#fff',
        borderRadius: '8px',
        border: '1px solid #fff',
        padding: '1px',
        boxShadow: 'rgba(0, 0, 0, 0.2) 0px 3px 1px -2px, rgba(0, 0, 0, 0.14) 0px 2px 2px 0px, rgba(0, 0, 0, 0.12) 0px 1px 5px 0px',


      },
      
      Circle:{
        display: 'inline-flex',
        width:'247px'
      },
      card: {
        width: '247px',
        display: 'inline-flex',
        flexWrap: 'unset',
  
      },
      circle: {
        paddingBottom: '9px',
        display: 'inline-flex',
        width:'257px',
        card: {
          width: '257px',
          display: 'inline-flex',
          flexWrap: 'unset',
    
        }
      },
      svg: {
        color: '#999',
        position: 'absolute',
        right: '10px',

      },
      field: {
        display: 'flex',
        float: 'left',
        alignItems: 'right', fontSize: '11px',
        width: '30px !important',
        maxWidth: '30px !important',
      },
      fields: {
        display: 'flex',
         float: 'left',
        alignItems: 'right', fontSize: '11px',
        width: '30px !important',
        maxWidth: '30px !important',
      },
      input: {
        border: '0px',
        borderBottom: '1px solid',
        margin: '0px 5px 0px 5px',
        padding: '0px 5px 0px 5px',
        width: '35px',
        alignItems: 'right', fontSize: '12px',
        
      },
      inputhex: {
        border: '0px',
        borderBottom: '1px solid',
        margin: '0px 5px 0px 5px',
        padding: '0px 5px 0px 5px',
        width: '70px',
        alignItems: 'right', fontSize: '12px',
      },
      label: {
        fontSize: '12px',
        color: '#999',
        padding: '0px 18px 0px 18px',
        alignItems: 'center',
      },
      saturation: {
        width: '100%',
        paddingBottom: '35%',
        position: 'relative',
        borderRadius: '2px 2px 0 0',
        overflow: 'hidden', borderRadius: '8px 8px 0 0',
      },

      Saturation: { radius: '4px' },
      controls: {
        display: 'flex',
      },
      color: {
        width: '32px',
      },
      swatch: {
        marginTop: '6px',
        width: '16px',
        height: '16px',
        borderRadius: '8px',
        position: 'relative',
        overflow: 'hidden',
      },
      active: {
        absolute: '0px 0px 0px 0px',
        borderRadius: '8px',
        boxShadow: 'inset 0 0 0 1px rgba(0,0,0,.1)',
        background: `rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, ${rgb.a})`,
        zIndex: '2',
      },
      toggles: {
        flex: '1',
      },
      hue: {
        height: '26px',
        position: 'relative',
        marginBottom: '10px',
        borderRadius: '8px',

      },
      Hue: {
        borderRadius: '0 0 8px 8px ',
        radius: '8px',

      },
      alpha: {
        height: '10px',
        position: 'relative',
      },
      Alpha: {
        radius: '2px',
      }, pointer: {
        position: 'absolute',
        zIndex: '1',
        boxSizing: 'border-box',
        width: '26px',
        height: '26px',
        transform: 'translate(-50%,-50%)',
        background: `rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, ${rgb.a})`,

        border: '2px solid #fff',
        borderRadius: '50%',
        boxSshadow: '0 2px 4px rgb(0 0 0 / 20%)',
      },
      slider: {
        position: 'absolute',
        zIndex: '1',
        boxSizing: 'border-box',
        width: '26px',
        height: '26px',
        border: '2px solid #fff',
        borderRadius: '50%',
        boxSshadow: '0 2px 4px rgb(0 0 0 / 20%)',
      },
    },
    'disableAlpha': {
      color: {
        width: '22px',
      },
      alpha: {
        display: 'none',
      },
      hue: {
        marginBottom: '1px',
        
      },

      swatch: {
        width: '10px',
        height: '10px',
        marginTop: '0px',
      },
    },
  }, passedStyles), { disableAlpha })


  let fields
  if (view === 'hex') {
    fields = (<div style={styles.fields} className="flexbox-fix">
      <div style={styles.field}>
        <EditableInput
          style={{ input: styles.inputhex, label: styles.label }}
          label="hex" value={hex}
          onChange={onChange}
        />
      </div>
    </div>)
  } else if (view === 'rgb') {
    fields = (<div style={styles.fields} className="flexbox-fix">
      <div style={styles.field}>
        <EditableInput
          style={{ input: styles.input, label: styles.label }}
          label="r"
          value={rgb.r}
          onChange={handleChangeRGB}
        />
      </div>
      <div style={styles.field}>
        <EditableInput
          style={{ input: styles.input, label: styles.label }}
          label="g"
          value={rgb.g}
          onChange={handleChangeRGB}
        />
      </div>
      <div style={styles.field}>
        <EditableInput
          style={{ input: styles.input, label: styles.label }}
          label="b"
          value={rgb.b}
          onChange={handleChangeRGB}
        />
      </div>
    </div>)
  }

  const showHighlight = (e) => {
    e.currentTarget.style.background = 'black'
  }
  const CustomPointer = () => {
    return (
      <div style={styles.pointer} />
    )
  }
  const CustomSlider = () => {
    return (
      <div style={styles.slider} />
    )
  }
  const hideHighlight = (e) => {
    e.currentTarget.style.background = 'grey'
  }

  return (
    <div style={styles.container}>
      <div style={styles.circle}>
        <Circle
          style={styles.Circle}
          circleSize={23}
          colors={['#FF0600', '#CB6CE6', '#8C52FF', '#464EF7', '#38B6FF', '#44DC39', '#FF914D', '#FFDE59']}
          circleSpacing={9}
          className='circle'
          width='257px'
          onChange={onChange}
        />
      </div>
      <div style={styles.saturation}>
        <Saturation
          style={styles.Saturation}
          hsl={hsl}
          hsv={hsv}
          pointer={CustomPointer}
          onChange={onChange}
        />
      </div>
      <div style={styles.hue}>
        <Hue hsl={hsl} onChange={onChange} pointer={CustomSlider}  style={styles.Hue}/>
      </div>

      <div style={{ display: 'flex', textAlign: 'right', 'padding': '0px' }}>
        <div style={{ display: 'flex', textAlign: 'left', 'padding': '0px' }}>{fields}</div>
        <div style={styles.toggles}>
          <div style={styles.icon} onClick={toggleViews} ref={(icon) => icon = icon}>
            <UnfoldMoreHorizontalIcon
              style={styles.svg}
              onMouseOver={showHighlight}
              onMouseEnter={showHighlight}
              onMouseOut={hideHighlight}
            />
          </div>
        </div>
      </div>
    </div>
  )
}

export default CustomPicker(MyPicker)

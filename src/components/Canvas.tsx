import React, { useState, useEffect, useRef, useLayoutEffect, forwardRef, useImperativeHandle } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import { CropperSource } from './CropperSource';
import Transform from '../utils/Transform';

interface CanvasProps {
  width?: number;
  height?: number;
  src?: string;
  style?: any;
  backgroundColor?: any;
  sourceDimension: { width: number, height: number };
}

export interface canvasDLRef {
  downloadImage: () => void;
}

const Canvas = forwardRef<canvasDLRef, CanvasProps>((props: CanvasProps, ref) => {
  const [originalImage, setOriginalImage] = useState(new window.Image());
  const [cache, setCache] = useState(new window.Image());
  const canvasRef = useRef<HTMLCanvasElement | null>(null);
  const [position, setPosition] = useState({ x: 0, y: 0 });
  const [scaledSize, setScaledSize] = useState({ width: 0, height: 0 });
  const [equal, setEqual] = useState(0);
  const imageRef = useRef<HTMLImageElement>(null);
  const canvasDLRef = useRef<HTMLCanvasElement | null>(null);
  const [backgroundColor, setBackgroundColor] =  useState('transparent');

  
  const downloadImage = () => {
    const canvas_dl = canvasDLRef.current;
    const canvas_source = canvasRef.current;
    const image = imageRef.current;

    if (canvas_dl && image && canvas_source) {
      const ctx = canvas_dl.getContext('2d', { alpha: false });
        
      const imageSize = {
        width: image.width,
        height: image.height,
      };
       
      const canvasSize = {
        width: image.width,
        height: image.width,
      };

      if (ctx) {
        ctx.clearRect(0, 0, canvasSize.width, canvasSize.height);
        ctx.canvas.height = imageSize.height;
        ctx.canvas.width = imageSize.width;
        const scaledSize = Transform.constrainProportions_um(imageSize, imageSize);
        const position = Transform.centerRect(scaledSize, imageSize);
        setPosition(position);
        setScaledSize(scaledSize);
        if (backgroundColor && backgroundColor !== 'transparent') {
            ctx.fillStyle = backgroundColor;
            ctx.fillRect(0, 0, canvas_dl.width, canvas_dl.height);
            ctx.globalCompositeOperation = 'source-over';
          }
        ctx.drawImage(image, position.x, position.y, scaledSize.width, scaledSize.height);

        const url = canvas_dl.toDataURL('image/png');
        const link = document.createElement('a');
        link.download = 'filename.png';
        link.href = url;
        link.click();
        drawImage();
      }
    } else {
      console.error('Canvas reference is invalid');
    }
  };

  const drawImage = () => {
    const image = imageRef.current;
    const canvas = canvasRef.current;

    const canvasSize = {
      width: props.width || 720,
      height: props.height || 480,
    };

    if (canvas && image && image.complete) {
      const ctx = canvas.getContext('2d', { alpha: true });

      const imageSize = {
        width: image.naturalWidth,
        height: image.naturalHeight,
      };

      if (ctx) {
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        const scaledSize = Transform.constrainProportions(imageSize, canvasSize);
        const position = Transform.centerRect(scaledSize, canvasSize);
        setPosition(position);
        setScaledSize(scaledSize);

        if (props.backgroundColor && props.backgroundColor !== 'transparent') {
          ctx.fillStyle = props.backgroundColor;
          ctx.fillRect(position.x, position.y, scaledSize.width, scaledSize.height);
          ctx.globalCompositeOperation = 'source-over';
          setBackgroundColor(props.backgroundColor);
        } else {
          ctx.clearRect(0, 0, canvas.width, canvas.height);
          ctx.globalCompositeOperation = 'source-over';
          setBackgroundColor('transparent');
        }

        ctx.drawImage(image, position.x, position.y, scaledSize.width, scaledSize.height);
        ctx.save();
      }
    }
  };

  useLayoutEffect(() => {
    drawImage();
  }, [props.src, props.backgroundColor]);

  useEffect(() => {
    if (props.src === cache.src && equal === 1) {
      console.log('equal');
      
    } else {
      if (props.src && equal === 0) {
        setEqual(1);

        const loadImage = async () => {
          const image = await Transform.loadBitmapData(props.src);
          setOriginalImage(image);
          setCache(image);
        };

        loadImage().catch(console.error);
      }
    }
  }, [props.src]);

  useImperativeHandle(ref, () => ({
    downloadImage: downloadImage
  }));
  const styles = {
    dl_canvas: {
        with: 0,
        height: 0,
        display:'none',
        visibilty:'hidden'
    },
    hidden:{
        with: 0,
        height: 0,
        display:'none',
        visibilty:'hidden'
    }
}
  return (
    <>
      <canvas
        key={`canvas`}
        width={props.width}
        height={props.height}
        ref={canvasRef}
        className={cn('adjustable-image-element', 'className')}
        style={props.style}
        id="user-canvas-image-without-background"
      />
      <CropperSource
        key={`-img`}
        ref={imageRef}
        className={'adjustable-image-source'}
        src={props.src}
        crossOrigin={'anonymous'}
        onLoad={drawImage}
        style={styles.hidden}
      />
      <canvas
        key={`canvas_dl`}
        ref={canvasDLRef}
        style={styles.hidden}
        id="user-canvas-image-without-background-download"
      />
    </>
  );
});

Canvas.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  src: PropTypes.string,
  style: PropTypes.any,
  backgroundColor: PropTypes.any,
  sourceDimension: PropTypes.shape({
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
  }).isRequired,
};

export default Canvas;

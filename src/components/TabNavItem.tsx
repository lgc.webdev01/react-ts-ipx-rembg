import React from "react";

type BookProps = {
    activeTab: number;
    title: string;
    id: number;
    setActiveTab: (id: number) => void;
  };
const TabNavItem:React.FunctionComponent <BookProps> = (props)=> {
    const { id, title, activeTab, setActiveTab }  = props;
 
 const handleClick = () => {
   setActiveTab(id);
 };
 
return (
   <li onClick={handleClick} className={activeTab === id ? "active" : ""}>
     { title }
   </li>
 );
};
export default TabNavItem;

import React, { useState } from "react";
import reactCSS from 'reactcss'
import MyPicker from "./MyPicker";

const ColorPicker: React.FC<{
                              value: string;
                              defaultColor?: string;
                              onChange: (val: string) => void;
                            }> = ({ value, onChange, defaultColor }) => {

  const [color, setColor] = useState('#FFFFFF');
  const [view, setView] = useState<string>('rgb');


  const toggleViews = () => {
    if (view === 'hex') {
      setView('rgb');
    } else if (view === 'rgb') {
      setView('hex');
    }
  };
  const handleColorChange = (color: any) => {
    console.log(color);
  
    
    setColor(color.hex);
    onChange(color.hex);
  };
  const styles = reactCSS({
    'default': {
      picker: {
        borderRadius: '8px',
        width: '241px !important',
        margin: 'auto',
        backgroundColor: '#FFFFFF',
        paddingBottom: '25%',
      },

      container: {
        backgroundColor: '#fff',
        borderRadius: '8px',
        border: '1px solid #fff',
        padding: '1px',
      }
    }
  });
  return (

    <div style={{
      position: "relative", backgroundColor: '#fff',
      borderRadius: '8px',
      border: '1px solid #fff',
      padding: '1px', alignItems: 'center'
    }}>
      <MyPicker
        color={color}
        onChangeComplete={handleColorChange}
        onChange={handleColorChange}
        view={view}
        pointer={true}
        toggleViews={toggleViews} />

    </div>

  );
};

export default ColorPicker;

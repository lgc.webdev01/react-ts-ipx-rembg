import React, { RefObject, useEffect, useRef, useState } from 'react';
import { useNavigate as useHistory } from 'react-router-dom';
import HomeContent from '../parts/HomeContent';
import Header from './Header';
import './Home.css'
import Footer from './Footer';

function Home() {
  const [image, setImage] = useState<File | null>();
  const [error, setError] = useState<string | null>(null);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const history = useHistory();
  const inputRef: RefObject<HTMLInputElement> = useRef(null);

  const handleFileUpload = (
    files: FileList | null,
    setError: (err: string) => void,
    setImage: (img: File) => void,
    history: any
  ) => {

    if (files && files.length > 0) {
      const file = files[0];
      let img = new Image()
      let img_size = { width: 0, height: 0 }
      img.src = window.URL.createObjectURL(file)
      img.onload = () => {
        img_size.width = img.width;
        img_size.height = img.height;
        try {
          if (!file) {
            setError('Please select an image');
          } else if (!file.type.startsWith('image')) {
            setError('Invalid file type. Select an image file');
          } else if (file.size > 125000000) {
            setError(
              'File is too large. Select an image below 5MB'
            );
          } else {
            setImage(file); // Navigate to the processing route
            history('/remove-background', { state: { image: file, dimension: img_size } });
          }
        } catch (error) {
          console.log(error);
          setError('An error occurred while uploading the image');
        } finally {
          setIsSubmitting(false); // Hide the loading spinner
        }
      }

    }
  };

  const handleClick = () => {
    // 👇️ open file input box on click on drag-drop
    if (inputRef.current) {
      inputRef.current.click();
    }
  };
  const handleFileUploadDemo = (event: {
    [x: string]: any; preventDefault: () => void; stopPropagation: () => void;
  }) => {
    event.preventDefault();
    event.stopPropagation();

    // console.log(event.currentTarget.id);
    // handleFileUpload(files, setError, setImage, history);
    history('/remove-background', { state: { id: event.currentTarget.id } });
  };
  const handleUpload = async (e: { target: { files: FileList | null } }) => {
    setError(null); // Reset any previous errors
    setIsSubmitting(true); // Show the loading spinner
    // const file = e.target?.files[0];
    const { files } = e.target;
    handleFileUpload(files, setError, setImage, history);
  };
  useEffect(() => {

  }, []);
  return (
    <>  <Header /><div>
      {' '}
      <main id="page">
        <div className="container v2 container-main flex">
          <div className="page-col_left">
            <h1>
              The <span>#1</span> One-Click Background Remover
            </h1>
            <img
              src="https://www.inpixio.com/remove-background/images/new/example.png"
              width="769"
              height="505"
              alt="Remove background" />
            <div className="ratings">
              <div className="rating">
                <a
                  href="https://www.trustpilot.com/review/inpixio.com"
                  target="_blank"
                  rel="noreferrer"
                >
                  <img
                    width="145"
                    height="68"
                    src="https://www.inpixio.com/remove-background/images/new/logo-trustpilot.svg"
                    alt="Trustpilot" />
                </a>
              </div>
              <div className="rating">
                <a
                  href="https://www.capterra.com/p/213175/inPixio-Photo-Studio/"
                  target="_blank"
                  rel="noreferrer"
                >
                  <img
                    width="145"
                    height="53"
                    src="https://www.inpixio.com/remove-background/images/new/logo-capterra.svg"
                    alt="Capterra" />
                </a>
              </div>
            </div>
          </div>
          <div className="page-col_mid">
            <img
              src="https://www.inpixio.com/remove-background/images/new/icon-arrow.svg"
              width="150"
              height="80"
              alt="background eraser" />
          </div>
          <div className="page-col_right">
            <div className="loading">
              <div className="loading__wrap">
                <div className="c-upload">
                  <div className="c-upload__local">
                    <p className="c-upload__drag file-drop" onClick={handleClick} id="drag-drop">
                      <img
                        src="https://www.inpixio.com/remove-background/images/new/icon-file.svg"
                        width="479"
                        height="277"
                        alt="Upload" />
                    </p>
                    <div className="c-upload__choose">
                      <label className="upload__button">
                        <b className="plus"></b>
                        <span>UPLOAD YOUR PHOTO</span>

                        <input
                          type="file"
                          className="file-choose"
                          onChange={handleUpload}
                          disabled={isSubmitting}
                          ref={inputRef} />
                      </label>
                    </div>
                  </div>
                </div>
                {error ? (
                  <p id="error-message" className="error-message">
                    <img
                      src="/remove-bg/ad0e6f0b-77d4-4ebb-aa3b-36e707a5da4e.png"
                      alt="Upload" /> {error}
                  </p>
                ) : <p>&nbsp;</p>}
                <div className="default-images flex">
                  <div className="default-images_label">
                    No image? <br />Try one of these :
                  </div>
                  <div className="default-images_select">
                    <a href="#" onClick={handleFileUploadDemo} className="example-image" id="Woman">
                      <img src="https://inpixio-removebg-app.s3.amazonaws.com/remove-bg/static/images/example-portrait.jpg" width="60" height="60" alt="example portrait" /></a>
                    <a href="#" onClick={handleFileUploadDemo} className="example-image" id="Camera">
                      <img src="https://inpixio-removebg-app.s3.amazonaws.com/remove-bg/static/images/example-products.jpg" width="60" height="60" alt="example product" /></a>
                    <a href="#" onClick={handleFileUploadDemo} className="example-image" id="Car">
                      <img src="https://inpixio-removebg-app.s3.amazonaws.com/remove-bg/static/images/example-car.jpg" width="60" height="60" alt="example car" /></a>
                    <a href="#" onClick={handleFileUploadDemo} className="example-image" id="Dog">
                      <img src="https://inpixio-removebg-app.s3.amazonaws.com/remove-bg/static/images/example-dog.jpg" width="60" height="60" alt="example dog" /></a>
                  </div>

                </div>
                <div className="flex">
                  <small className='privacy'>
                    By uploading an image you agree to our {' '}
                    <a href="https://www.inpixio.com/terms-conditions/" target="_blank">
                      Terms of Service </a> and{' '}
                    <a href="https://www.inpixio.com/privacy-policy/" target="_blank">
                      Privacy Policy
                    </a>
                    .
                  </small>
                </div>
              </div>

            </div>

          </div>
        </div>
      </main>

    </div>
    <Footer/>  </>
  );
}
export default Home;

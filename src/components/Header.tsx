import React, { useEffect, useState } from 'react';
import Cookies from 'js-cookie';
import { useLocation, useNavigate as useHistory } from 'react-router-dom';
 


const Header = () => {

    const [btnLoading, setBtnLoading] = useState(false);
    const [email, setEmail] = useState(false);
    const [password, setPassword] = useState('');
    const [showUpgradeButton, setShowUpgradeButton] = useState(false);
    const [showLoginButton, setShowLoginButton] = useState(false);
    const location = useLocation();
    const [isAuthenticated, setIsAuthenticated] = useState(false);
    const currentauth = false;
    useEffect(() => {
        const getProductsByToken = async () => {
            try {
              var url = "https://myaccount.inpixio.com/api/account/owner-products"
              const address = await fetch(url, {
                method: "GET",
                mode: 'cors',
                credentials: 'include',
                redirect: "follow"
              })
                .then((response) => {
                  if (response?.status === 400) {
      
                    return { error: true }
                  } else {
                    return response.json();
                  }
      
                })
                .then(data => {
                  if (data?.error) {
                    throw new Error("Status code error :" + data.error)
                  } else {
                    const filteredProducts = data.filter((product: any) =>
                      ['P031528', 'P031533', 'P031534', 'P031523'].includes(product.uid)
                    ).filter((product: any) => new Date(product.endDate) > new Date());
                    setEmail(true)
                    setIsAuthenticated(true);
                    return filteredProducts;
                  }
      
                })
                .catch(function (error) {
                  return error
                });
      
      
              const printAddress = () => {
                address.then((a: any) => {
                  return (a);
                });
              };
      
              return printAddress();
            } catch (e: any) {
            }
          }
      
      
          getProductsByToken();
      
        
        if (location.pathname === '/remove-background' && currentauth ===false) {
            setShowUpgradeButton(true);
        } else {
            setShowUpgradeButton(false);
        }
        if (location.pathname !== '/cart') {
            setShowLoginButton(true);
        } else {
            setShowLoginButton(false);
        }
        
    }, [location]);

    const onSubmit = async (event: React.SyntheticEvent) => {
        event.preventDefault();
        setBtnLoading(true);
        try {

        } catch (e) {

        } finally {
            setBtnLoading(false);
        }
    };

    return (
        <header id="header">
            <div className="container v2 flex">
            <a href="/remove-bg"><img
              src="https://www.inpixio.com/remove-background/images/new/logo-inpixio.svg"
              width="168"
              height="52"
              alt="inPixio"
              className="logo" />
          </a>
                <div className="header-menu">
                    <div className="header-menu_pro">
                        {showUpgradeButton === true && <a href="" className="btn outline pro-upg">
                            <img src="https://www.inpixio.com/rb-tool/assets/img/icon-pro.svg" width="20" height="18" alt="UPGRADE TO PRO" className="inline icon" />
                            <span className="inline">UPGRADE TO PRO</span>
                        </a>}

                    </div>
                    {showLoginButton === true &&
                        <a href="https://myaccount.inpixio.com/" className="account-btn" target="_self">
                            <img src="https://www.inpixio.com/rb-tool/assets/img/icon-account.svg" width="24" height="24" alt="Account Icon" />
                            {email === false && <span className="account-btn_status"></span> }
                            
                        </a>}
                </div>
            </div>
        </header>
    );
};

export default Header;
/*

const Header = () => {
  return (<header id="header">
      <div className="container v2 flex">
          <a href="/"><img
              src="https://www.inpixio.com/remove-background/images/new/logo-inpixio.svg"
              width="168"
              height="52"
              alt="inPixio" />
          </a>
      </div>
  </header>);
};
export default Header;
*/


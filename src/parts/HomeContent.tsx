import React from 'react';

 

const HomeContent = () => {

    return (
    <div className="main" style={{ paddingTop: 40 }}>
        <section className="effortless">
            <div className="container">
                <div className="row eff-cars">
                    <div className="big-block">
                        <div className="eff-image">
                            <img width="1200" height="815" src="https://www.inpixio.com/mb/assets/latest/image-back/eff/eff.webp" alt="Compare before" />
                        </div>
                    </div>
                    <div className="lil-block">
                        <div className="eff-title">Remove Backgrounds Effortlessly</div>
                        <div className="eff-text">
                            <p>
                                Create marketplace-ready professional photos in record time with AI-powered background removal.
                            </p>
                            <p>
                                Get a transparent background automatically. Then easily add a white background to showcase your product and maximize sales.
                            </p>
                        </div>
                        <div className="try-but">
                            <a href="https://webtools.inpixio.com/download.cfm?go=https://filecdn.inpixio.com/Photo_Studio/ML/inPixio_Photo_Studio_TR.exe">Try for free</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section className="alltabs">
            <div className="container tabs">
                <div className="eff-title">Precision Cut-Out For All Your Pictures</div>
                <div className="eff-text">Powerful AI detects the subject of your photo and generates a transparent PNG automatically</div>
                <ul className="tabs__caption">
                    <li className="act">Cars</li>
                    <li>People</li>
                    <li>Animals</li>
                    <li>Products</li>
                    <li>Graphics</li>
                </ul>
                <div className="tabs-display">
                    <div className="tabs__content fst act">
                        <div className="compare-block">
                            <img width="1200" height="700" src="https://www.inpixio.com/mb/assets/latest/image-back/compare-car/car-before.jpg" alt="Compare car before" />
                            <img width="1200" height="700" src="https://www.inpixio.com/mb/assets/latest/image-back/compare-car/car-after.jpg" alt="Compare car after" />
                        </div>
                    </div>
                    <div className="tabs__content scnd">
                        <div className="compare-block">
                            <img width="1200" height="700" src="https://www.inpixio.com/mb/assets/latest/image-back/compare-yoga/yoga-before.jpg" alt="Compare yoga before" />
                            <img width="1200" height="700" src="https://www.inpixio.com/mb/assets/latest/image-back/compare-yoga/yoga-after.jpg" alt="Compare yoga after" />
                        </div>
                    </div>
                    <div className="tabs__content trd">
                        <div className="compare-block">
                            <img width="1200" height="700" src="https://www.inpixio.com/mb/assets/latest/image-back/compare-dog/dog-before.jpg" alt="Compare dog before" />
                            <img width="1200" height="700" src="https://www.inpixio.com/mb/assets/latest/image-back/compare-dog/dog-after.jpg" alt="Compare dog after" />
                        </div>
                    </div>
                    <div className="tabs__content trd">
                        <div className="compare-block">
                            <img width="1200" height="700" src="https://www.inpixio.com/mb/assets/latest/image-back/compare-product/product-before.jpg" alt="Compare product before" />
                            <img width="1200" height="700" src="https://www.inpixio.com/mb/assets/latest/image-back/compare-product/product-after.jpg" alt="Compare product after" />
                        </div>
                    </div>
                    <div className="tabs__content trd">
                        <div className="compare-block">
                            <img width="1200" height="700" src="https://www.inpixio.com/mb/assets/latest/image-back/compare-graf/graf-before.jpg" alt="Compare graf before" />
                            <img width="1200" height="700" src="https://www.inpixio.com/mb/assets/latest/image-back/compare-graf/graf-after.jpg" alt="Compare graf after" />
                        </div>
                    </div>
                </div>
                <div className="try-but">
                    <a href="https://webtools.inpixio.com/download.cfm?go=https://filecdn.inpixio.com/Photo_Studio/ML/inPixio_Photo_Studio_TR.exe">Try for free</a>
                </div>
            </div>
        </section>
        <section className="why">
            <div className="container">
                <div className="eff-title">Why Choose inPixio’s Background Remover?</div>
                <div className="row">
                    <div className="why-block">
                        <div className="why-image">
                            <img loading="lazy" width="230" height="230" src="https://www.inpixio.com/mb/assets/latest/image-back/why/why1.png" alt="Automated image" />
                        </div>
                        <div className="why-title">
                            100% Automated
                        </div>
                        <div className="why-text">
                            Simply upload & erase backgrounds in just one click. Replace backgrounds with color backgrounds or custom backgrounds easily thanks to our powerful AI technology.
                        </div>
                        <div className="why-arrow1">
                            <img loading="lazy" width="126" height="71" src="https://www.inpixio.com/mb/assets/latest/image-back/why/why1-arrow.svg" />
                        </div>
                    </div>
                    <div className="why-block">
                        <div className="why-image">
                            <img loading="lazy" width="230" height="230" src="https://www.inpixio.com/mb/assets/latest/image-back/why/why3.png" alt="Automated image" />
                        </div>
                        <div className="why-title">
                            Save Time & $$$
                        </div>
                        <div className="why-text">
                            Whether you’re a small business with limited time & resources, or an employee with other priorities, you can edit and download your finished PNG image in no time.
                        </div>
                        <div className="why-arrow2">
                            <img loading="lazy" width="153" height="81" src="https://www.inpixio.com/mb/assets/latest/image-back/why/why2-arrow.svg" />
                        </div>
                    </div>
                    <div className="why-block">
                        <div className="why-image">
                            <img loading="lazy" width="230" height="230" src="https://www.inpixio.com/mb/assets/latest/image-back/why/why2.png" alt="Automated image" />
                        </div>
                        <div className="why-title">
                            No Experience Required
                        </div>
                        <div className="why-text">
                            No need to be a professional designer to change backgrounds. Our easy-to-use tools will do the heavy lifting like magic, so you can focus on what really matters.
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section className="testimals">
            <div className="container">
                <div className="testimals-wrap">
                    <div className="testimals-car owl-carousel">
                        <div className="test-block">
                            <div className="left-block">
                                <div className="inic">GH</div>
                                <div className="info">
                                    <div className="name">Gunter Helmholz</div>
                                    <div className="date">Mar 28, 2022</div>
                                    <div className="stars">
                                        <img width="145px" height="26px" src="https://www.inpixio.com/mb/assets/latest/image-back/stars-45.svg" alt="Trustpilot raiting 4.5 stars" />
                                    </div>
                                </div>
                            </div>
                            <div className="right-block">
                                <div className="test-title">inPixio is a great software for photography editing</div>
                                <div className="test-text">inPixio works perfectly on background elimination. I love it and would recommend it to any photographers.</div>
                            </div>
                        </div>
                        <div className="test-block">
                            <div className="left-block">
                                <div className="inic pink">RC</div>
                                <div className="info">
                                    <div className="name">Richard Custer</div>
                                    <div className="date">Nov 12, 2021</div>
                                    <div className="stars five">
                                        <img width="145px" height="26px" src="https://www.inpixio.com/mb/assets/latest/image-back/stars-5.svg" alt="Trustpilot raiting 5 stars" />
                                    </div>
                                </div>
                            </div>
                            <div className="right-block">
                                <div className="test-title">Excellent Software</div>
                                <div className="test-text">Excellent change the background software! Very easy to use for even beginners.</div>
                            </div>
                        </div>
                        <div className="test-block">
                            <div className="left-block">
                                <div className="inic orange">FC</div>
                                <div className="info">
                                    <div className="name">Fazlin Calvert</div>
                                    <div className="date">Oct 18, 2021</div>
                                    <div className="stars five">
                                        <img width="145px" height="26px" src="https://www.inpixio.com/mb/assets/latest/image-back/stars-5.svg" alt="Trustpilot raiting 5 stars" />
                                    </div>
                                </div>
                            </div>
                            <div className="right-block">
                                <div className="test-title">I absolutely love this program</div>
                                <div className="test-text">I absolutely love this program. It is an extremely useful tool for my small business. I cannot live without it.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section className="dog">
            <div className="container">
                <div className="row">
                    <div className="big-block">
                        <div className="dog-photo">
                            <img loading="lazy" width="1017" height="642" src="https://www.inpixio.com/mb/assets/latest/image-back/dance/dance.jpg" alt="Dance People"/>
                            <img className="romb" src="https://www.inpixio.com/mb/assets/latest/image/romb.svg" alt="romb"/>
                        </div>
                    </div>
                    <div className="lil-block">
                        <div className="eff-title">Try inPixio’s AI background remover today</div>
                        <div className="eff-text">Save time creating professional images with inPixio Photo Studio, your rapid one-click background remover.</div>
                        <div className="buy-buts">
                            <div className="try-but">
                                <a href="https://webtools.inpixio.com/download.cfm?go=https://filecdn.inpixio.com/Photo_Studio/ML/inPixio_Photo_Studio_TR.exe">Try for free</a>
                            </div>
                            <div className="try-but buy-but">
                                <a href="https://store.inpixio.com/clickgate/join.aspx?ref=inpixio.upclick.com%2F1&ujid=R8bije3CCAU%3D&step=2&mkey3=web_direct-buy">BUY NOW</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    );
};

export default HomeContent;
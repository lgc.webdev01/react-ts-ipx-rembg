type PageViewEvent = {
    event: 'page_view';
    pagePath: string;
  };
  
  type InViewEvent = {
    event: 'inView';
    label: string;
  };
  
  type ClickEvent = {
    event: 'click';
    label: string;
  };
  
  export type DataLayerType =
    | PageViewEvent
    | InViewEvent
    | ClickEvent
  
  export const pushDataLayer = (data: DataLayerType): void => {
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push(data);
  };
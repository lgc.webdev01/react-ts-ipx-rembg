export default class Transform {
  static storeImageSourceSize(sourceDimension) {
    return {
      width: sourceDimension.width,
      height: sourceDimension.height,
    };
  }
  static fillCanvas(ctx, color) {
    // ctx.globalCompositeOperation = 'destination-over';
     
    ctx.fillStyle = color;
    ctx.globalCompositeOperation = 'source-over';
    ctx.fillRect(0, 0, 720, 480);

  }
  static MAX_WIDTH = 720;
  static MAX_HEIGHT = 480;
  static calculateSize(img, maxWidth = Transform.MAX_WIDTH, maxHeight = Transform.MAX_HEIGHT) {
    let width = img.width;
    let height = img.height;

    // calculate the width and height, constraining the proportions
    if (width > height) {
      if (width > maxWidth) {
        if (width < Transform.MAX_WIDTH) {
          width = Transform.MAX_WIDTH;
          //newHeight = Math.ceil(imageSize.height * (newWidth / imageSize.width));
        }
        height = Math.round((height * maxWidth) / width);
        width = maxWidth;
      }
    } else {
      if (height > width) {

        if (height < Transform.MAX_HEIGHTh) {
          height = Transform.MAX_HEIGHT;
          //newHeight = Math.ceil(imageSize.height * (newWidth / imageSize.width));
        }
        width = Math.round((width * maxHeight) / height);
        height = maxHeight;
      }
    }
    // return [width, height];
    return {
      width: width,
      height: height,
    };
  }

  static constrainProportions_um(imageSize, canvasSize = { width: 0, height: 0 }) {
    const minRatio = Math.min(imageSize.height / imageSize.height, imageSize.width / imageSize.width);
    const width = imageSize.width * minRatio;
    const height = imageSize.height * minRatio;
    const soureWidth = canvasSize.width;
    const sourceHeight = canvasSize.height;
    if (isNaN(height)) {
      console.log('here');

    }
     
    let newWidth = width;
    let newHeight = height;
  
    // Calculate the scaling factor for width and height
    const scaleFactorWidth = soureWidth / imageSize.width;
    const scaleFactorHeight = sourceHeight / imageSize.height;

    // Choose the smaller scaling factor to maintain proportion
    const scaleFactor = Math.min(scaleFactorWidth, scaleFactorHeight);

    // Calculate the new dimensions of the scaled image
    const scaledWidth = Math.round(imageSize.width * scaleFactor);
    const scaledHeight = Math.round(imageSize.height * scaleFactor);

    return {
      width: Math.floor(scaledWidth),
      height: Math.floor(scaledHeight),
    };
  }

  static constrainProportions(imageSize, canvasSize = { width: 0, height: 0 }) {
    const minRatio = Math.min(imageSize.height / imageSize.height, imageSize.width / imageSize.width);
    const width = imageSize.width * minRatio;
    const height = imageSize.height * minRatio;
    const soureWidth = canvasSize.width;
    const sourceHeight = canvasSize.height;
    if (isNaN(height)) {
      console.log('here');

    }
   
    let newWidth = width;
    let newHeight = height;
    if (newWidth > newHeight) {

      if (newWidth <= soureWidth) {
        newWidth = soureWidth;
        newHeight = Math.ceil(imageSize.height * (newWidth / imageSize.width));
      } else {
        // Calculate new height while maintaining aspect ratio
        newHeight = newHeight * (soureWidth / newWidth);
        // Set width to maximum width
        newWidth = soureWidth;
      }
    }
    else {
      if (newHeight <= sourceHeight) {
        newHeight = sourceHeight;
        newWidth = Math.ceil(imageSize.width * (newHeight / imageSize.height));
      } else {
        // Calculate new height while maintaining aspect ratio
        newWidth = newWidth * (sourceHeight / newHeight);
        // Set width to maximum width
        newHeight = sourceHeight;
      }
    }
    // Calculate the scaling factor for width and height
    const scaleFactorWidth = 720 / imageSize.width;
    const scaleFactorHeight = 480 / imageSize.height;

    // Choose the smaller scaling factor to maintain proportion
    const scaleFactor = Math.min(scaleFactorWidth, scaleFactorHeight);

    // Calculate the new dimensions of the scaled image
    const scaledWidth = Math.round(imageSize.width * scaleFactor);
    const scaledHeight = Math.round(imageSize.height * scaleFactor);

    return {
      width: Math.floor(scaledWidth),
      height: Math.floor(scaledHeight),
    };
  }

  static constrainProportions3(img_width, img_height) {

    const maxWidth = 720;
    const maxHeight = 480;
    const aspectRatio = img_width / img_height;

    let newWidth = img_width;
    let newHeight = img_height;
    // calculate the width and height, constraining the proportions
    if (newWidth > newHeight) {
      // Check if the image is wider than the maximum width
      if (newWidth > maxWidth) {
        // Calculate new height while maintaining aspect ratio
        newHeight = newHeight * (maxWidth / newWidth);
        // Set width to maximum width
        newWidth = maxWidth;
      } else if (newWidth < maxWidth) {
        // Set width to maximum width
        newWidth = maxWidth;
        // Calculate new height while maintaining aspect ratio
        newHeight = newWidth / aspectRatio;
      }
    } else {
      if (newHeight > maxHeight) {
        // Calculate new height while maintaining aspect ratio
        newWidth = newWidth * (maxHeight / newHeight);
        // Set width to maximum width
        newHeight = maxHeight;
      } else if (newHeight < maxHeight) {
        // Set width to maximum width
        newHeight = maxHeight;
        // Calculate new height while maintaining aspect ratio
        newWidth = newHeight / aspectRatio;
      }

    }

    const scaledWidth = newWidth;
    const scaledHeight = newHeight;
    return {
      width: scaledWidth,
      height: scaledHeight,
    };
  }
  static constrainProportions2(from, to, maxWidth = Transform.MAX_WIDTH, maxHeight = Transform.MAX_HEIGHT) {
    const minRatio = Math.min(to.height / from.height, to.width / from.width);
    let newWidth = from.width;
    let newHeight = from.height;
    if (newWidth > newHeight) {
      if (newWidth < Transform.maxWidth) {
        newWidth = Transform.maxWidth;
        newHeight = Math.ceil(from.height * (newWidth / from.width));
      }
    }
    else {
      if (newHeight < Transform.maxHeight) {
        newHeight = Transform.maxHeight;
        newWidth = Math.ceil(from.width * (newHeight / from.height));
      }
    }

    const scaledWidth = newWidth > to.width ? newWidth * minRatio : newWidth;
    const scaledHeight = newHeight > to.height ? newHeight * minRatio : newHeight;
    return {
      width: scaledWidth,
      height: scaledHeight,
    };
  }
  static centerRect(rect, container) {
     
    return {
      x: (container.width * 0.5) - (Math.ceil(rect.width) * 0.5),
      y: (container.height * 0.5) - (Math.ceil(rect.height) * 0.5),
    };
  }
  static clearCanvas(canvas, ctx) {
    if (!canvas || !ctx) {
      return;
    }
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.save();
  }

  static renderImage(ctx, img, position, boundRect) {

    ctx.drawImage(img, position.x, position.y, boundRect.width, boundRect.height);
    // ctx.save();
  }
  
  static renderCentered(ctx, image, imgRect, boundRect) {
    const scaledRect = Transform.constrainProportions(imgRect, boundRect);
    const position = Transform.centerRect(scaledRect, boundRect);

    Transform.renderImage(ctx, image, position, scaledRect);
  }
  static loadBitmapDataOld(source) {
    return new Promise((resolve, reject) => {
      try {
        const img = new Image();
        img.crossOrigin = 'Anonymous';
        img.onload = () => {
          const size = Transform.calculateSize(img);
          img.width = size.width;
          img.height = size.height;
          resolve(img);
        };
        img.src = source;
      } catch (er) {
        console.log(er);


        reject(er);
      }
    });
  }
  static loadBitmapData(source) {
    return new Promise((resolve, reject) => {
      try {
        const img = new Image();
        img.crossOrigin = 'Anonymous';
        img.onload = () => {
          /* const maxWidth = 720;
           const aspectRatio = img.width / img.height;
   
           let width = img.width;
           let height = img.height;
           // calculate the width and height, constraining the proportions
           if (width > height) {
             // Check if the image is wider than the maximum width
             if (width > maxWidth) {
               // Calculate new height while maintaining aspect ratio
               height = height * (maxWidth / width);
               // Set width to maximum width
               width = maxWidth;
             } else if (width < maxWidth) {
               // Set width to maximum width
               width = maxWidth;
               // Calculate new height while maintaining aspect ratio
               height = width / aspectRatio;
             }
           }  else {
             if (height > maxHeight) {
               // Calculate new height while maintaining aspect ratio
               width = width * (maxHeight / height);
               // Set width to maximum width
               height = maxHeight;
             } else if (height < maxHeight) {
               // Set width to maximum width
               height = maxHeight;
               // Calculate new height while maintaining aspect ratio
               width = height / aspectRatio;
             }
 
           }*/

          // Set new width and height values for the image
          // img.width = width;
          // img.height = height;
          const size = Transform.calculateSize(img);
          img.width = size.width;
          img.height = size.height;
          resolve(img);
        };
        img.src = source;
      } catch (error) {
        console.error(error);
        reject(error);
      }
    });
  }


  static getRealDimensions(cropRect, imgRect) {
    return {
      x: (cropRect.x - imgRect.x) / imgRect.width,
      y: (cropRect.y - imgRect.y) / imgRect.height,
      width: cropRect.width / imgRect.width,
      height: cropRect.height / imgRect.height,
    };
  }
}
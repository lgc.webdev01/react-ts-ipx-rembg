interface Options {
    format?: 'image/png' | 'image/jpeg';
    quality?: number;
    width?: number;
    height?: number;
    Canvas?: typeof window.CanvasRenderingContext2D;
    Image?: typeof window.Image;
    crossOrigin?: string | null;
  }
  
 
  // Defaults
const defaultOptions = {
    format: 'image/png',
    quality: 0.92,
    width: undefined,
    height: undefined,
    Canvas: undefined,
    crossOrigin: undefined
  };
  
  type Source = {
    src: string;
    img: HTMLImageElement;
    opacity?: number;
    x?: number;
    y?: number;
  };
  
  // Return Promise
  const mergeImages = (sources: (string | Source)[], options: any = {}) =>
    new Promise<string>((resolve) => {
      options = Object.assign({}, defaultOptions, options);
  
      // Setup browser/Node.js specific variables
      const canvas: HTMLCanvasElement = options?.Canvas
        ? new options.Canvas()
        : window.document.createElement('canvas');
      const Image = options?.Image || window.Image;
      const images = sources.map(source => new Promise<Source>((resolve, reject) => {
        // Convert sources to objects
        let sourceObj: Source;
        if (typeof source === 'string') {
          sourceObj = { src: source, img: new Image() };
        } else {
          sourceObj = { ...source, img: new Image() };
        }
      
        // Resolve source and img when loaded
        sourceObj.img.crossOrigin = options.crossOrigin;
        sourceObj.img.onerror = () => reject(new Error(`Couldn't load image ${sourceObj.src}`));
        sourceObj.img.onload = () => resolve(sourceObj);
        sourceObj.img.src = sourceObj.src;
      }));

      /*const images = sources.map(source => new Promise((resolve, reject) => {
        // Convert sources to objects

        if (typeof source === 'string') {
          source = { src: source };
        }
        // Add type guard to ensure that source.src is always a string
        if (typeof source.src !== 'string') {
          return reject(new Error('Image source must be a string'));
        }
      
        // Resolve source and img when loaded
        const img = new Image();
        img.crossOrigin = options.crossOrigin;
        img.onerror = () => reject(new Error('Couldn\'t load image'));
        img.onload = () => resolve(Object.assign({}, source, { img }));
        img.src = source.src;
      }));
*/
      // Load sources
  
      // When sources have loaded
      Promise.allSettled(images).then((results) => {
        // Get canvas context
        const ctx = (canvas instanceof HTMLCanvasElement) ? canvas.getContext('2d') : canvas;
      
        const loadedImages: Source[] = results
          .filter((result) => result.status === 'fulfilled')
          .map((result) => (result as PromiseFulfilledResult<Source>).value);
        
        // Draw images to canvas
        if (ctx) {
            loadedImages.forEach((image) => {
            ctx.globalAlpha = image.opacity ? image.opacity : 1;
            ctx.drawImage(image.img, image.x || 0, image.y || 0);
            });
        }
        if (options.Canvas && options.format === 'image/jpeg') {
            // Resolve data URI for node-canvas jpeg async
            return new Promise((resolve, reject) => {
                canvas.toDataURL(options.format, {
                    quality: options.quality,
                    progressive: false
                  });
            });
          }
      
        // Resolve all other data URIs sync
        return canvas.toDataURL(options.format, options.quality);
      })
    });
  
  export default mergeImages;

  /*
  const mergeImages = (sources: Array<Source | string>, options: Options = {}): Promise<string> => {
    options = Object.assign({}, defaultOptions, options);
  
    const canvas = options.Canvas ? new options.Canvas() : document.createElement('canvas');
    const Image = options.Image || window.Image;
    const ctx = (canvas instanceof HTMLCanvasElement) ? canvas.getContext('2d') : canvas;
  
    const images = sources.map(source => {
      if (typeof source === 'string') {
        source = { src: source };
      }
  
      return new Promise<Source>(resolve => {
        const img = new Image();
        // img.crossOrigin = options.crossOrigin;
        img.onerror = () => resolve({ src: '' });
        img.onload = () => resolve(Object.assign({}, source, { img }));
        img.src = source.src;
      });
    });
  
    return new Promise(resolve => {
      Promise.all(images).then(images => {
        const getSize = dim => options[dim] || Math.max(...images.map(image => image.img[dim]));
        canvas.width = getSize('width');
        canvas.height = getSize('height');
  
        images.forEach(image => {
          ctx.globalAlpha = image.opacity ? image.opacity : 1;
          return ctx.drawImage(image.img, image.x || 0, image.y || 0);
        });
  
        if (options.Canvas && options.format === 'image/jpeg') {
          canvas.toDataURL(options.format, {
            quality: options.quality,
            progressive: false
          }, (err, jpeg) => {
            if (err) {
              throw err;
            }
            resolve(jpeg);
          });
        } else {
          resolve(canvas.toDataURL(options.format, options.quality));
        }
      });
    });
  };
  
  export default mergeImages;*/
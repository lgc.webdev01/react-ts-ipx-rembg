import { client, ResponseAPI,client_um } from '../api/client';
import { useEffect, useState } from 'react';
export const createPost = async (file: File): Promise<any> => {
  return new Promise((resolve, reject) => {
    try {
      const fd = new FormData();
      fd.append('image', file, file.name);
      client.post('', fd)
        .then((data) => {
          resolve(data);
        })
        .catch((error) => {
          throw error;
        });
    } catch (error) {
      reject(error);
    }
  });
};

export const useCachedCreatePost = async (file: File): Promise<any> => {
  return new Promise(async (resolve, reject) => {
    try {
      const fd = new FormData();
      fd.append('image', file, file.name);
      const data = await client_um.post('', fd);
      await resolve(data);
    } catch (error) {
      reject(error);
    }
  });
};

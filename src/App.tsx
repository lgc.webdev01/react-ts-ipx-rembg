
import React, { useCallback, useContext, useEffect, useState } from 'react'
import './index.css';
import { Route, Routes } from 'react-router-dom';
// import { AuthContext, AuthProvider} from './api/AuthContext';
import Home from './components/Home';
import MainAppFrame from './components/MainAppFrame';
import Cart from './components/Cart';
// import NotFound from './components/NotFound';

import { useGEo } from './api/auth';


import Accounts from './components/Accounts';
// import Header from './components/Header';
// import Header from './components/Header';
// import Login from './components/Login';


const App: React.FC = () => {
  const language = useGEo();
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [isProductActived, setIsProductActived] = useState(false);
  const [products, setProducts] = useState([] as any);
  const [error, setError] = useState(null);
  const setAuthPersistent = useCallback(setIsAuthenticated, [setIsAuthenticated]);

  useEffect(() => {
    const getProductsByToken = async () => {
      try {
        var url = "https://myaccount.inpixio.com/api/account/owner-products"
        const address = await fetch(url, {
          method: "GET",
          mode: 'cors',
          credentials: 'include',
          redirect: "follow"
        })
          .then((response) => {
            if (response?.status === 400) {

              return { error: true }
            } else {
              return response.json();
            }

          })
          .then(data => {
            if (data?.error) {
              throw new Error("Status code error :" + data.error)
            } else {
              const filteredProducts = data.filter((product: any) =>
                ['inPixio Remove Background', 'inPixio Photo Studio 12 Pro'].includes(product.name)
              ).filter((product: any) => new Date(product.endDate) > new Date());
              // console.log({ data })
              // console.log({ fi:filteredProducts })
              if (filteredProducts && filteredProducts.length > 0) {
                // setProducts(filteredProducts);
                setIsProductActived(true);
              }

              setAuthPersistent(true);
              setIsAuthenticated(true);
              return filteredProducts;
            }

          })
          .catch(function (error) {

            setError(error);
            setAuthPersistent(false);
            return error
          });


        const printAddress = () => {
          address.then((a: any) => {
            return (a);
          });
        };

        return printAddress();
      } catch (e: any) {
        setError(e)
      }
    }


    getProductsByToken();

  }, []);


  return (
    <>
      
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/remove-background-trial" element={<MainAppFrame />} />
        <Route path={`/remove-background`} element={isProductActived ? <Accounts /> : <MainAppFrame />} />
        <Route path="/cart" element={<Cart language={language} />} />
        <Route element={<MainAppFrame />} />
      </Routes>
    </>


  );
};
export default App;
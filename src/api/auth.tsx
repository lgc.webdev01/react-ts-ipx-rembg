import Cookies from 'js-cookie';
import { useState, useEffect } from 'react';

export const useIsAuthenticated = () => {
  const access_token = localStorage.getItem("access_token");
  if (access_token) {
    getProductsByToken();
    console.log(access_token);
    return true;
  } else {
    return false;
  }
};


export function getAccessToken() {
  /* const login = new URLSearchParams({
     'grant_type': 'password',
     'username': 'dvalide@avanquest.com',
     'password': 'XenakyS1314@!',
     'client_id': 'web',
     'visitorId': 'ea24624e-71e1-4323-b90e-7a84a994c112'
 
   });
   return  fetch("https://oauth.inpixio.com/api/token", {
     method: "POST",
     mode: 'cors',
     body: login
   }).then((response) => {
       console.log({response})
       return response.json();
   })
   .then(data => {
     console.log({data})
     Cookies.set('access_token', data.access_token);
     Cookies.set('refresh_token', data.refresh_token);
     Cookies.set('Identity', data.firstName + ' '+ data.lastName);
       return data;
   })
   .catch(function(error) {
       console.log(error)
   });*/
  /*Cookies.set('access_token', "3yoBSIJgaLR6kwmWwaUn7wfcfENt0DfbaPwI2qPYthMrfRPgoisp-FCH7aeTWRwftJvcGHik0999Yu3Uv8Gtje0CKwXwj20IAvOxnstYbdUT67jHpNOH3M6ueO7nKqtA3KwWGH-2vWco2AiKgQlXm7ppsw9GEuW_ozljJWywxf8OLykOH-toEHiP_0QN5HuwAzXEiSb00C9WmgW8bsGie3apBSLvSoauZAUP35gQjM29oPsY_UgCt6MQOgNwYBm7uF-llMPjlJ2lnYQHNCScPw6kC1TVM0agjyGrrEqPZybJ6yuSypGklAmDxHGJ2z9yRz2p_sk2pGWI7RQrwUOBg5qO-wM7toRtwUYidCmQZ80i1T6usCsNVaN32sfiBDJd4roqJFAZ5B9iZF70qfvmwGH6VMoTnEuJHi6Wt7sccZc7opJ5");
    Cookies.set('refresh_token', "1342ea4027994fe089857e606a1e0f78");
    Cookies.set('Identity',' DANIEL VALIDE');
    Cookies.set('oauth_status',1);
    Cookies.set('oauth_externalClient','facebook');*/

  //.then(data => data.json())
}
export const getProductsByToken = () => {

  var url = "https://myaccount.inpixio.com/api/account/owner-products"
  const address = fetch(url, {
    method: "GET",
    mode: 'cors',
    credentials : 'include',
    redirect: "follow"
  })
    .then((response) => {
      console.log({ response })
      if (response?.status === 400 ) {
        throw new Error("Status code error :" + response.status)
        return  {
          isAuthenticated: true,
          products: [{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"1eed7a35-5724-4775-8ad6-a0f67e01bf07","name":"inPixio Remove Background","unitName":"inpixio-remove-bg-monthly","uid":"P031528","isFree":false,"group":0,"plan":"1-Day Plan","purchaseDate":"2023-02-07T08:31:40Z","endDate":"2023-02-08T08:31:40Z","expiresIn":26520,"status":3520,"modules":[{"name":"remove bg"}],"order":0,"graceDate":"2023-02-08T08:31:40Z"},{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"6817aa40-50ab-4152-9625-18b80549aac0","spId":"U27221010021001322171","name":"inPixio Photo Studio","unitName":"inpixio-ps-yearly","uid":"P030791","isFree":false,"group":0,"plan":"Yearly Plan","purchaseDate":"2022-10-10T06:10:01Z","endDate":"2023-10-10T00:00:00Z","nextRebillDate":"2023-10-10T00:00:00Z","expiresIn":21077420,"status":3456,"modules":[{"name":"cutter"},{"name":"eraser"}],"order":1,"graceDate":"2023-10-18T00:00:00Z"},{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"c7be79e6-9eaa-410c-ba42-af8fa632559b","spId":"U27230203080805068814","name":"inPixio Photo Studio Pro","unitName":"inpixio-ps-pro-yearly","uid":"P030792","isFree":false,"group":0,"pcs":1,"plan":"Yearly Plan","purchaseDate":"2023-02-03T13:08:05Z","endDate":"2024-02-03T00:00:00Z","nextRebillDate":"2024-02-03T00:00:00Z","expiresIn":31099820,"status":3456,"modules":[{"name":"cutter"},{"name":"editor"},{"name":"eraser"}],"order":2,"graceDate":"2024-02-11T00:00:00Z"},{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"2bc0527e-7565-4219-8711-421d10d10161","spId":"UXXXXXXXXXXXXXXX","name":"inPixio Photo Studio Pro","unitName":"inpixio-ps-pro-yearly","uid":"P030792","isFree":false,"group":0,"pcs":1,"plan":"Yearly Plan","purchaseDate":"2021-06-16T04:40:46Z","endDate":"2022-04-30T00:00:00Z","nextRebillDate":"2022-04-30T00:00:00Z","expiresIn":0,"status":3203,"modules":[{"name":"cutter"},{"name":"editor"},{"name":"eraser"}],"order":3,"graceDate":"2022-05-08T00:00:00Z"},{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"a9ca424c-0695-48ec-b97e-a76920d84934","spId":"U27210617004129960393","name":"inPixio Photo Studio","unitName":"inpixio-ps-yearly","uid":"P030791","isFree":false,"group":0,"plan":"Yearly Plan","purchaseDate":"2021-06-17T04:40:14Z","endDate":"2022-06-17T00:00:00Z","nextRebillDate":"2022-06-17T00:00:00Z","expiresIn":0,"status":3203,"modules":[{"name":"cutter"},{"name":"eraser"}],"order":4,"graceDate":"2022-06-25T00:00:00Z"},{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"8c690f30-b6b0-47b4-be31-15a01cb31691","spId":"U27210621055622016975","name":"inPixio Photo Studio","unitName":"inpixio-ps-yearly","uid":"P030791","isFree":false,"group":0,"plan":"Yearly Plan","purchaseDate":"2021-06-21T09:55:29Z","endDate":"2022-06-21T00:00:00Z","nextRebillDate":"2022-06-21T00:00:00Z","expiresIn":0,"status":3203,"modules":[{"name":"cutter"},{"name":"eraser"}],"order":5,"graceDate":"2022-06-29T00:00:00Z"}]
      }}
      return response.json();
    })
    .then(data => {
      if (data?.error) {
        throw new Error("Status code error :" + data.error)
        return  {
          isAuthenticated: true,
          products: [{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"1eed7a35-5724-4775-8ad6-a0f67e01bf07","name":"inPixio Remove Background","unitName":"inpixio-remove-bg-monthly","uid":"P031528","isFree":false,"group":0,"plan":"1-Day Plan","purchaseDate":"2023-02-07T08:31:40Z","endDate":"2023-02-08T08:31:40Z","expiresIn":26520,"status":3520,"modules":[{"name":"remove bg"}],"order":0,"graceDate":"2023-02-08T08:31:40Z"},{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"6817aa40-50ab-4152-9625-18b80549aac0","spId":"U27221010021001322171","name":"inPixio Photo Studio","unitName":"inpixio-ps-yearly","uid":"P030791","isFree":false,"group":0,"plan":"Yearly Plan","purchaseDate":"2022-10-10T06:10:01Z","endDate":"2023-10-10T00:00:00Z","nextRebillDate":"2023-10-10T00:00:00Z","expiresIn":21077420,"status":3456,"modules":[{"name":"cutter"},{"name":"eraser"}],"order":1,"graceDate":"2023-10-18T00:00:00Z"},{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"c7be79e6-9eaa-410c-ba42-af8fa632559b","spId":"U27230203080805068814","name":"inPixio Photo Studio Pro","unitName":"inpixio-ps-pro-yearly","uid":"P030792","isFree":false,"group":0,"pcs":1,"plan":"Yearly Plan","purchaseDate":"2023-02-03T13:08:05Z","endDate":"2024-02-03T00:00:00Z","nextRebillDate":"2024-02-03T00:00:00Z","expiresIn":31099820,"status":3456,"modules":[{"name":"cutter"},{"name":"editor"},{"name":"eraser"}],"order":2,"graceDate":"2024-02-11T00:00:00Z"},{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"2bc0527e-7565-4219-8711-421d10d10161","spId":"UXXXXXXXXXXXXXXX","name":"inPixio Photo Studio Pro","unitName":"inpixio-ps-pro-yearly","uid":"P030792","isFree":false,"group":0,"pcs":1,"plan":"Yearly Plan","purchaseDate":"2021-06-16T04:40:46Z","endDate":"2022-04-30T00:00:00Z","nextRebillDate":"2022-04-30T00:00:00Z","expiresIn":0,"status":3203,"modules":[{"name":"cutter"},{"name":"editor"},{"name":"eraser"}],"order":3,"graceDate":"2022-05-08T00:00:00Z"},{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"a9ca424c-0695-48ec-b97e-a76920d84934","spId":"U27210617004129960393","name":"inPixio Photo Studio","unitName":"inpixio-ps-yearly","uid":"P030791","isFree":false,"group":0,"plan":"Yearly Plan","purchaseDate":"2021-06-17T04:40:14Z","endDate":"2022-06-17T00:00:00Z","nextRebillDate":"2022-06-17T00:00:00Z","expiresIn":0,"status":3203,"modules":[{"name":"cutter"},{"name":"eraser"}],"order":4,"graceDate":"2022-06-25T00:00:00Z"},{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"8c690f30-b6b0-47b4-be31-15a01cb31691","spId":"U27210621055622016975","name":"inPixio Photo Studio","unitName":"inpixio-ps-yearly","uid":"P030791","isFree":false,"group":0,"plan":"Yearly Plan","purchaseDate":"2021-06-21T09:55:29Z","endDate":"2022-06-21T00:00:00Z","nextRebillDate":"2022-06-21T00:00:00Z","expiresIn":0,"status":3203,"modules":[{"name":"cutter"},{"name":"eraser"}],"order":5,"graceDate":"2022-06-29T00:00:00Z"}]
      }
      }else{
        console.log({ data })
        return  {
          isAuthenticated: true,
          products: [{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"1eed7a35-5724-4775-8ad6-a0f67e01bf07","name":"inPixio Remove Background","unitName":"inpixio-remove-bg-monthly","uid":"P031528","isFree":false,"group":0,"plan":"1-Day Plan","purchaseDate":"2023-02-07T08:31:40Z","endDate":"2023-02-08T08:31:40Z","expiresIn":26520,"status":3520,"modules":[{"name":"remove bg"}],"order":0,"graceDate":"2023-02-08T08:31:40Z"},{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"6817aa40-50ab-4152-9625-18b80549aac0","spId":"U27221010021001322171","name":"inPixio Photo Studio","unitName":"inpixio-ps-yearly","uid":"P030791","isFree":false,"group":0,"plan":"Yearly Plan","purchaseDate":"2022-10-10T06:10:01Z","endDate":"2023-10-10T00:00:00Z","nextRebillDate":"2023-10-10T00:00:00Z","expiresIn":21077420,"status":3456,"modules":[{"name":"cutter"},{"name":"eraser"}],"order":1,"graceDate":"2023-10-18T00:00:00Z"},{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"c7be79e6-9eaa-410c-ba42-af8fa632559b","spId":"U27230203080805068814","name":"inPixio Photo Studio Pro","unitName":"inpixio-ps-pro-yearly","uid":"P030792","isFree":false,"group":0,"pcs":1,"plan":"Yearly Plan","purchaseDate":"2023-02-03T13:08:05Z","endDate":"2024-02-03T00:00:00Z","nextRebillDate":"2024-02-03T00:00:00Z","expiresIn":31099820,"status":3456,"modules":[{"name":"cutter"},{"name":"editor"},{"name":"eraser"}],"order":2,"graceDate":"2024-02-11T00:00:00Z"},{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"2bc0527e-7565-4219-8711-421d10d10161","spId":"UXXXXXXXXXXXXXXX","name":"inPixio Photo Studio Pro","unitName":"inpixio-ps-pro-yearly","uid":"P030792","isFree":false,"group":0,"pcs":1,"plan":"Yearly Plan","purchaseDate":"2021-06-16T04:40:46Z","endDate":"2022-04-30T00:00:00Z","nextRebillDate":"2022-04-30T00:00:00Z","expiresIn":0,"status":3203,"modules":[{"name":"cutter"},{"name":"editor"},{"name":"eraser"}],"order":3,"graceDate":"2022-05-08T00:00:00Z"},{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"a9ca424c-0695-48ec-b97e-a76920d84934","spId":"U27210617004129960393","name":"inPixio Photo Studio","unitName":"inpixio-ps-yearly","uid":"P030791","isFree":false,"group":0,"plan":"Yearly Plan","purchaseDate":"2021-06-17T04:40:14Z","endDate":"2022-06-17T00:00:00Z","nextRebillDate":"2022-06-17T00:00:00Z","expiresIn":0,"status":3203,"modules":[{"name":"cutter"},{"name":"eraser"}],"order":4,"graceDate":"2022-06-25T00:00:00Z"},{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"8c690f30-b6b0-47b4-be31-15a01cb31691","spId":"U27210621055622016975","name":"inPixio Photo Studio","unitName":"inpixio-ps-yearly","uid":"P030791","isFree":false,"group":0,"plan":"Yearly Plan","purchaseDate":"2021-06-21T09:55:29Z","endDate":"2022-06-21T00:00:00Z","nextRebillDate":"2022-06-21T00:00:00Z","expiresIn":0,"status":3203,"modules":[{"name":"cutter"},{"name":"eraser"}],"order":5,"graceDate":"2022-06-29T00:00:00Z"}]
      }
        return data;
      }
     
    })
    .catch(function (error) {
      console.log(error);
      return  {
        isAuthenticated: true,
        products: [{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"1eed7a35-5724-4775-8ad6-a0f67e01bf07","name":"inPixio Remove Background","unitName":"inpixio-remove-bg-monthly","uid":"P031528","isFree":false,"group":0,"plan":"1-Day Plan","purchaseDate":"2023-02-07T08:31:40Z","endDate":"2023-02-08T08:31:40Z","expiresIn":26520,"status":3520,"modules":[{"name":"remove bg"}],"order":0,"graceDate":"2023-02-08T08:31:40Z"},{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"6817aa40-50ab-4152-9625-18b80549aac0","spId":"U27221010021001322171","name":"inPixio Photo Studio","unitName":"inpixio-ps-yearly","uid":"P030791","isFree":false,"group":0,"plan":"Yearly Plan","purchaseDate":"2022-10-10T06:10:01Z","endDate":"2023-10-10T00:00:00Z","nextRebillDate":"2023-10-10T00:00:00Z","expiresIn":21077420,"status":3456,"modules":[{"name":"cutter"},{"name":"eraser"}],"order":1,"graceDate":"2023-10-18T00:00:00Z"},{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"c7be79e6-9eaa-410c-ba42-af8fa632559b","spId":"U27230203080805068814","name":"inPixio Photo Studio Pro","unitName":"inpixio-ps-pro-yearly","uid":"P030792","isFree":false,"group":0,"pcs":1,"plan":"Yearly Plan","purchaseDate":"2023-02-03T13:08:05Z","endDate":"2024-02-03T00:00:00Z","nextRebillDate":"2024-02-03T00:00:00Z","expiresIn":31099820,"status":3456,"modules":[{"name":"cutter"},{"name":"editor"},{"name":"eraser"}],"order":2,"graceDate":"2024-02-11T00:00:00Z"},{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"2bc0527e-7565-4219-8711-421d10d10161","spId":"UXXXXXXXXXXXXXXX","name":"inPixio Photo Studio Pro","unitName":"inpixio-ps-pro-yearly","uid":"P030792","isFree":false,"group":0,"pcs":1,"plan":"Yearly Plan","purchaseDate":"2021-06-16T04:40:46Z","endDate":"2022-04-30T00:00:00Z","nextRebillDate":"2022-04-30T00:00:00Z","expiresIn":0,"status":3203,"modules":[{"name":"cutter"},{"name":"editor"},{"name":"eraser"}],"order":3,"graceDate":"2022-05-08T00:00:00Z"},{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"a9ca424c-0695-48ec-b97e-a76920d84934","spId":"U27210617004129960393","name":"inPixio Photo Studio","unitName":"inpixio-ps-yearly","uid":"P030791","isFree":false,"group":0,"plan":"Yearly Plan","purchaseDate":"2021-06-17T04:40:14Z","endDate":"2022-06-17T00:00:00Z","nextRebillDate":"2022-06-17T00:00:00Z","expiresIn":0,"status":3203,"modules":[{"name":"cutter"},{"name":"eraser"}],"order":4,"graceDate":"2022-06-25T00:00:00Z"},{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"8c690f30-b6b0-47b4-be31-15a01cb31691","spId":"U27210621055622016975","name":"inPixio Photo Studio","unitName":"inpixio-ps-yearly","uid":"P030791","isFree":false,"group":0,"plan":"Yearly Plan","purchaseDate":"2021-06-21T09:55:29Z","endDate":"2022-06-21T00:00:00Z","nextRebillDate":"2022-06-21T00:00:00Z","expiresIn":0,"status":3203,"modules":[{"name":"cutter"},{"name":"eraser"}],"order":5,"graceDate":"2022-06-29T00:00:00Z"}]
    }
    });

  const printAddress = () => {
    address.then((a) => {
     return(a);
    });
  };

  return printAddress();

}
export const getProductByToken2 = async () => {
  return new Promise(async (resolve, reject) => {
   // const access_token = Cookies.get('access_token');
    const access_token = localStorage.getItem("access_token")
    var headers = { "Authorization": "Bearer " + access_token }
    var url = "https://myaccount.inpixio.com/api/account/owner-products"
    try {
      const data = await fetch(url, {
        method: "GET",
        mode: 'cors',
        headers: headers,
        redirect: "follow"
      }).then((response) => {
        console.log({ response })
        return response.json();
      })
        .then(data => {
          console.log({ data })

          if (!data.ok) {

            throw new Error("Status code error :" + data)

          }
          if (data?.error) {
            throw new Error("Status code error :" + data.status)
            return false;
          }
          if (data) {
            const filteredData = data.filter((product: { spId: string; }) => product.spId === "U27221215141900257889");
            console.log(filteredData);
            return filteredData;
          }

        })
        .catch(function (error) {
          // console.log(error)
          throw new Error("Status code error :" + error)
          return false;
        });
      await resolve(data);
    } catch (error) {
      reject(error);
    }
  });
};

export const getProductByToken = () => {
  // const access_token = Cookies.get('access_token');
  const access_token = localStorage.getItem("access_token")
  if (access_token) {

    console.log({ access_token })
    var headers = { "Authorization": "Bearer " + access_token }
    var url = "https://myaccount.inpixio.com/api/account/owner-products"
    return fetch(url, {
      method: "GET",
      mode: 'cors',
      headers: headers,
      redirect: "follow"
    })
      .then((response) => {
        console.log({ response })
        return response.json();
      })
      .then(data => {
        console.log({ data })
        if (!data.ok) {

          throw new Error("Status code error :" + data)

        }
        if (data?.error) {
          throw new Error("Status code error :" + data.status)
          return false;
        }
        if (data) {
          const filteredData = data.filter((product: { spId: string; }) => product.spId === "U27221215141900257889");
          console.log(filteredData);
          return data;
        }

      })
      .catch(function (error) {
        // console.log(error)
        return false;
      });

  } else {
    return false;
  }

}


export const useThirdPartyCookieCheck = () => {

  const [isSupported, setIsSupported] = useState(false);

  useEffect(() => {
    const frame = document.createElement("iframe");
    frame.id = "3pc";
    frame.src = "https://myaccount.inpixio.com/account/products/"; //Add your hosted domain url here
    frame.style.display = "block";
    frame.style.position = "fixed";
    frame.style.width = "800px";
    frame.style.height = "800px";
    document.body.appendChild(frame);

    window.addEventListener(
      "message",
      function listen(event) {
        if (event.data === "3pcSupported" || event.data === "3pcUnsupported") {
          setIsSupported(event.data === "3pcSupported");
          //document.body.removeChild(frame);
          //window.removeEventListener("message", listen);
        }
      },
      false
    );
  }, []);

  return isSupported;
};

export const getUserGeoData = () => {
  return async function () {
    // Reset state.

    // Fetch geolocation data.
    const geolocation = await fetch("https://get.geojs.io/v1/ip/geo.json").then(
      res => res.json()
    );

  };
}

export const useGEo = () => {
  const defaultLanguage = Cookies.get('GEO') || 'en';
  return defaultLanguage
}
import React, { useContext, useEffect, useState } from "react";
import Cookies from "js-cookie";



const AuthContext = React.createContext(
  {} as {
    user: { products: string };
    authenticate: () => Promise<void>;
    isLoading: boolean;
    isAuthenticated: boolean;
    token: string;
  }
);

export const AuthProvider = (children: any ) => {
  const [user, setUser] = useState({ products: "" });
  const [isLoading, setIsLoading] = useState(false);
  const isAuthenticated = !!user;


  const authenticate = async () => {
    setIsLoading(true);
    try {
      const { data: user } = await fetch('https://myaccount.inpixio.com/api/account/owner-products',{
        method : "GET",
        mode: 'cors',
        credentials : 'include',
       redirect: "follow"
    }) .then((response) => {
        console.log({ response })
        return response.json();
      })
      .then(data => {
        console.log({ data })
        if (!data.ok) {

          throw new Error("Status code error :" + data)

        }
        if (data?.error) {
          throw new Error("Status code error :" + data.status)
          return  {
            isAuthenticated: true,
            products: [{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"1eed7a35-5724-4775-8ad6-a0f67e01bf07","name":"inPixio Remove Background","unitName":"inpixio-remove-bg-monthly","uid":"P031528","isFree":false,"group":0,"plan":"1-Day Plan","purchaseDate":"2023-02-07T08:31:40Z","endDate":"2023-02-08T08:31:40Z","expiresIn":26520,"status":3520,"modules":[{"name":"remove bg"}],"order":0,"graceDate":"2023-02-08T08:31:40Z"},{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"6817aa40-50ab-4152-9625-18b80549aac0","spId":"U27221010021001322171","name":"inPixio Photo Studio","unitName":"inpixio-ps-yearly","uid":"P030791","isFree":false,"group":0,"plan":"Yearly Plan","purchaseDate":"2022-10-10T06:10:01Z","endDate":"2023-10-10T00:00:00Z","nextRebillDate":"2023-10-10T00:00:00Z","expiresIn":21077420,"status":3456,"modules":[{"name":"cutter"},{"name":"eraser"}],"order":1,"graceDate":"2023-10-18T00:00:00Z"},{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"c7be79e6-9eaa-410c-ba42-af8fa632559b","spId":"U27230203080805068814","name":"inPixio Photo Studio Pro","unitName":"inpixio-ps-pro-yearly","uid":"P030792","isFree":false,"group":0,"pcs":1,"plan":"Yearly Plan","purchaseDate":"2023-02-03T13:08:05Z","endDate":"2024-02-03T00:00:00Z","nextRebillDate":"2024-02-03T00:00:00Z","expiresIn":31099820,"status":3456,"modules":[{"name":"cutter"},{"name":"editor"},{"name":"eraser"}],"order":2,"graceDate":"2024-02-11T00:00:00Z"},{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"2bc0527e-7565-4219-8711-421d10d10161","spId":"UXXXXXXXXXXXXXXX","name":"inPixio Photo Studio Pro","unitName":"inpixio-ps-pro-yearly","uid":"P030792","isFree":false,"group":0,"pcs":1,"plan":"Yearly Plan","purchaseDate":"2021-06-16T04:40:46Z","endDate":"2022-04-30T00:00:00Z","nextRebillDate":"2022-04-30T00:00:00Z","expiresIn":0,"status":3203,"modules":[{"name":"cutter"},{"name":"editor"},{"name":"eraser"}],"order":3,"graceDate":"2022-05-08T00:00:00Z"},{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"a9ca424c-0695-48ec-b97e-a76920d84934","spId":"U27210617004129960393","name":"inPixio Photo Studio","unitName":"inpixio-ps-yearly","uid":"P030791","isFree":false,"group":0,"plan":"Yearly Plan","purchaseDate":"2021-06-17T04:40:14Z","endDate":"2022-06-17T00:00:00Z","nextRebillDate":"2022-06-17T00:00:00Z","expiresIn":0,"status":3203,"modules":[{"name":"cutter"},{"name":"eraser"}],"order":4,"graceDate":"2022-06-25T00:00:00Z"},{"ownerEmail":"dv@crmweb.fr","allowed":1,"used":1,"id":"8c690f30-b6b0-47b4-be31-15a01cb31691","spId":"U27210621055622016975","name":"inPixio Photo Studio","unitName":"inpixio-ps-yearly","uid":"P030791","isFree":false,"group":0,"plan":"Yearly Plan","purchaseDate":"2021-06-21T09:55:29Z","endDate":"2022-06-21T00:00:00Z","nextRebillDate":"2022-06-21T00:00:00Z","expiresIn":0,"status":3203,"modules":[{"name":"cutter"},{"name":"eraser"}],"order":5,"graceDate":"2022-06-29T00:00:00Z"}]
        }
        }
        if (data) {
            const filteredProducts = data.filter((product: any) =>
            ['P031528', 'P030791'].includes(product.uid)
        ).filter((product: any) => new Date(product.endDate) > new Date());
        setUser({
            products: filteredProducts
        });
          return data;
        }

      })
      setUser(user);
      Cookies.set("token", "token");
    } catch (error) {
      console.log({ error });
      // setUser(null);
      Cookies.remove("token");
    }
    setIsLoading(false);
  };

  useEffect(() => {
    const token = Cookies.get("token");
    if (!token) return;
    authenticate();
  }, []);

  useEffect(() => {
    const Component = children.type;

    // If it doesn't require auth, everything's good.
    if (!Component.requiresAuth) return;

    // If we're already authenticated, everything's good.
    if (isAuthenticated) return;

    // If we don't have a token in the cookies, logout
    const token = Cookies.get("token");
    

    // If we're not loading give the try to authenticate with the given token.
    if (!isLoading) {
      authenticate();
    }
  }, [isLoading, isAuthenticated, children.type.requiresAuth]);

  return (
    <AuthContext.Provider
      value={{
        user,
        authenticate,
        isLoading,
        isAuthenticated: !!user,
        token: Cookies.get("token"),
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => useContext(AuthContext);
import axios from 'axios';

export const client = axios.create({
  baseURL: 'https://inpixio-remove-bg-v2-zceht2uy2q-ey.a.run.app/image/remove_bg',
});

export const client_um = axios.create({
  baseURL: 'https://inpixio-remove-bg-v2-zceht2uy2q-ey.a.run.app/image/remove_bg_um',
});
export interface ResponseAPI {
  mime: string;
  image: string;
  success: string;
}
